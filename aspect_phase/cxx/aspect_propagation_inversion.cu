using namespace std;
#undef max
#undef min

#include <stdio.h>
#include <stdlib.h>
#include<math.h>
#include <cuda.h>
#include <cublas.h>
#include<time.h>
#include"brent.hpp"
#include"auxiliary_macros.h"

#undef NDEBUG
#include<assert.h>


struct  AspectErrGradArgs_struct;
typedef struct AspectErrGradArgs_struct AspectErrGradArgs;


struct AspectSetup_struct {
  // this structure contains the geometrical setup, and physical parameters
  
  int size0;
  // vertical size of the imqge
  
  int size1;
  // horizontal size of the image
  
  float DBR_half;
  // nominal delta beta ratio, divided by two. Infact internally the  phase and absorption are used and
  // absorption is the amplitude squared
  
  float DB_EQUILIBRATION_FACTOR;
  // a preconditioning factor to equilibrate the huge numerical difference between d and b
  // in the optimisation gradients calculations 
  
  float *M_phase2shift;
  // This array will contain for each pixel a 2x2 array which converts the phase gradient into shifts.
  // The dependency over pixel position was used in cone geometry. In parallel geometry it could be reduced to a simple scalar factor.

  int verbose;
  // trigger verbosity

  float SOURCE_X;  // in pixels
  float SOURCE_Y;   // in pixels
  // the projection of the source point over the detector. Was used in cone geometry. Does not matter much in parallel geometry
  
  float SOURCE_DISTANCE ;
  // in meters
  
  float DETECTOR_DISTANCE ;
  // in meters
  
  float IMAGE_PIXEL_SIZE ;
  // in micron, detector pixel size. Not voxel size
  
  float K0 ;
  //this is 2pi over lamba(centimeters)
  
  // int n_spettro;
  // float * spettro_data;
  
  int OVS ;
  // for OVS>1 oversampling is done.
  // This feature has not been yet thoroughly tested. Therefore consider that its value is 1 ( no oversampling)
  
  int spectral_binning ;
  // if its value is 1, spectral points are considered separately, if it is 2 they will be considered two by two, and so on
  
};
typedef struct AspectSetup_struct AspectSetup;

struct AspectErrGradArgs_struct {
  // This structure is used for argument passing to the gradient routines.

  AspectSetup *exp_setup;
  // the experiment parameters structure declared just above.
  
  float *transm ;
  // the retrieved fictive transmitted beam, in other words the signal on the fictive detector just after the sample.

  float *  radio_data;
  // the  transmitted beam, in other words the signal on the detector.
  
  float *  flat_field;
  // The flat field. The signal without the sample
  
  double errore;
  // to return the error between forward model and the detector signal
  
  int n_spettro;
  // the number of spectral points that are considered for polychromaticity
  
  float * spettro_data;
// for every spectral point , four floats. These are :
  // - the ratio between spectral point energy and the nominal energy at which the nominal db ratio has been calculated
  // - the ratio between spectral point beta and the nominal beta at the nominal energy 
  // - the ratio between spectral point delta...
  // - the beam fractional intensity of the spectral point
      
};
typedef struct AspectErrGradArgs_struct AspectErrGradArgs;


struct cuErrGradArgs_struct {
  // this structure is used to pass arguments to functions which calculated the gradient on the gpu, calling kernels
  
  AspectSetup *exp_setup;
  // as above
  
  float *d_phase_and_logT ;
  // on GPU. This array contains a first part with the phase, and a second part with the logarithm of the intensity ( Transmission )
  
  float *d_phase_and_logT_buffer ;
  // on GPU. This is an auxiliary d_phase_and_logT, used as temporary in line minimisation

  
  int Npix;
  // an auxiliary to book-keep the number of pixels.
  
  float *  d_radio_data_ovs;
  // on GPU. The  transmitted beam, in other words the signal on the detector.
  
  float *   d_flat_field;
  // on GPU. The flat field. The signal without the sample

  float *    d_mGrad;
  // on GPU. Stores the negative of the gradient 

  
  float *   d_pos_y;
  float *   d_pos_x;
  // on GPU. Store the shifts in pixels

  float *   der_pos_y;
  float *   der_pos_x;
  // on GPU. The derivative of the error function relative to the shifts  at the nominal energy.

  
  float   d_pos_max_y;
  float   d_pos_max_x;
  // for hacking purposes only: used to retrieve the maximum values of shifts. Hard coding only.

  
  float *d_tmp_laplacian;
  // on GPU. Auxiliary array to store the result of the gradient transposed operator
  
  float *d_P_gc ;
  // The minimisation direction ( traditionally name p) of the conjugate gradient algorithm. On GPU.

  
  double errore;
  // used to store the error
  
  double * d_auxiliary_for_reduction;
  // auxiliary array on GPU which is used for sums reduction.
  
  double *d_one;
  // auxiliary vector containing one. This is used someimes as arguments to some cuda_blas routine
  
  float *d_Field_intensity;
  // this will contain the exponential of the transmission logarithm. In other words the transmission ( intensity for a flat field of one).
  // But the algorithm variable if the logarithm (logT  in phase_and_logT) while this is an auxiliary temporary. 

  
  float *d_M_phase2shift;
  // as M_phase2shift in   AspectSetup_struct but this one is on GPU.

  float *    d_mGrad_old;
  // auxiliary to store a previous value of a gradienbt
  
  float *    dmGda_along_p;
  // the derivative of the d_mGrad_ gradient with respect to variation of the alpha parameter which is a scalar parameter which, multiplying p_cg, parametrise the position
  // along the minimisation direction.

  
  float *d_BUFFER;
  // this is a blackboard, of the size of the image , when one can do things like adding the data, subtracting the forward model, and using the residuals
  // for calculating the gradient

  
  int dograd;
  // If also gradient has to be calculated, beside calculating the error. 

  float *d_Tmp_For_auxiliary_expansion;
  // an auxiliary array which is used insted the routine for  expansion/shrinking.


  int n_spettro;
  float * d_spettro_data;
  // as d_spettro_data in   AspectSetup_struct but this one is on GPU.
 
};
typedef struct cuErrGradArgs_struct cuErrGradArgs;

void init_M_phase2shift( AspectSetup *setup ) {
  // this function initialise the matrices M_phase2shift which transform at every pixel the derivatives of the phase into shifts
  
  setup->M_phase2shift = (float*) malloc(4*setup->size0*setup->size1*sizeof(float));
  
  float source_y =   setup->SOURCE_Y   ; 
  float source_x =   setup->SOURCE_X ;

  float  SOURCE_DISTANCE_pixels_x  =  setup->SOURCE_DISTANCE *1.0e6/ setup->IMAGE_PIXEL_SIZE  ;
  float  SOURCE_DISTANCE_pixels_y  =  setup->SOURCE_DISTANCE *1.0e6/ setup->IMAGE_PIXEL_SIZE  ;
  // distances in pixel unit/
  // Pixel size is in micron here. Distances in meters
  
  int iy;

  float fxx  ;
  float fyx  ;  
  float fxy  ;
  float fyy  ;
  // float K0 =  self->params.K0 ;   // essa e' in cm-1 e col 2pi  # bisogna dare ENERGY_KEV in input
  // float RENORM ;


  // printf(" in init\n");
  
  {
    float magni  = (setup->SOURCE_DISTANCE+setup->DETECTOR_DISTANCE)/(setup->SOURCE_DISTANCE);
    float L      = setup->DETECTOR_DISTANCE;
    float pixsz1 = setup->IMAGE_PIXEL_SIZE ;
    float pixsz2 = setup->IMAGE_PIXEL_SIZE ;
    
    fxx = magni/pixsz1 *  L* 1.0e6/setup->IMAGE_PIXEL_SIZE ;    
    fyx = magni/pixsz1 *  L* 1.0e6/setup->IMAGE_PIXEL_SIZE ;    
    fxy = magni/pixsz2 *  L* 1.0e6/setup->IMAGE_PIXEL_SIZE ;    
    fyy = magni/pixsz2 *  L* 1.0e6/setup->IMAGE_PIXEL_SIZE ;    

  }
  
  for(iy=0; iy<setup->size0; iy++) {
    int ix;

    float fact_y = 1.0 ; 
    for(ix=0; ix<setup->size1; ix++) {
      float fact=1.0;
      // spostato sul pso in elementwise

      float fact_x = 1;
      
      float Kx_su_Kz  = (ix - source_x)/ SOURCE_DISTANCE_pixels_x  ; 
      float Ky_su_Kz  = (iy - source_y)/ SOURCE_DISTANCE_pixels_y  ;

      float R = sqrt( 1 + (Kx_su_Kz)*(Kx_su_Kz) + (Ky_su_Kz)*(Ky_su_Kz) ) ; 
      
      float Kx =  Kx_su_Kz *   setup->K0 / R ;
      float Ky =  Ky_su_Kz *   setup->K0 / R ;
      float Kz =  1.0      *   setup->K0 / R ;
      
      // K's are in cm**-1
      
      fact = fact_y * fact_x  ; 
      
      setup->M_phase2shift[ 4*(    setup->size1 * iy  + ix ) + 0    ]  =   1.0/Kz *fyy *( 1.0  + Ky*Ky/Kz/Kz  ) *1.0e4*fact; // /RENORM;    
      setup->M_phase2shift[ 4*(    setup->size1 * iy  + ix ) + 1    ]  =   1.0/Kz *fyx *(        Ky*Kx/Kz/Kz  ) *1.0e4*fact; // /RENORM;
      setup->M_phase2shift[ 4*(    setup->size1 * iy  + ix ) + 2    ]  =   1.0/Kz *fxy *(        Kx*Ky/Kz/Kz  ) *1.0e4*fact; // /RENORM;
      setup->M_phase2shift[ 4*(    setup->size1 * iy  + ix ) + 3    ]  =   1.0/Kz *fxx *( 1.0  + Kx*Kx/Kz/Kz  ) *1.0e4*fact; // /RENORM;   //   lambda* L/pxs**2

    }
  }
} 



__global__ static void  CDV_kernel( float *F, float *Q , int N)  {
  // This Change Of Variable (CDV) Kernel transform the logarithm Q of the transmission into transmission ( intensity) F
  // The array F will be used in the tough kernels to simplify a little.
  int gid = blockDim.x * blockIdx.x + threadIdx.x ;
  float tmp;
  while ( gid < N )
    {
      tmp = Q[gid] ;
      if(tmp>2.0) {   // This to avoid pathological run-out. Transmission > 1 which go to infinity.
	tmp = 2.0;
	Q[gid] = tmp;
      }
      F[gid]  = __expf( tmp ) ;
      gid += blockDim.x * gridDim.x ;
    }
}

__global__ static void  multM_N_kernel( float *d_pos_y, float * d_pos_x,  float *MM ,  int size0, int size1)  {
  // this Kernel multiplies at every pixel a two component vectors by the MM matrix, in place.
  // The postfix N means "Normal" to distinguish it from the transpose operation. ( In general, the derivative of a quadratic form
  // gives expressions containing some operator and their tranposed )
  
  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;
  
  int ix = 16*bidx+tidx ; 
  int iy = 16*bidy+tidy ;
  int ipix = iy*(size1+1)+ix; 
  if( ix< size1+1 && iy < size0+1) {

    int ipix_m = min( iy  , size0 - 1 )*(size1)+min( ix, size1 - 1); 
    
    float a1,a2;
    a1 = d_pos_y[ipix  ];
    a2 = d_pos_x[ipix  ];

    float m11, m12, m21, m22 ; 

    m11 = MM[4*ipix_m +0] ; 
    m12 = MM[4*ipix_m +1] ; 
    m21 = MM[4*ipix_m +2] ; 
    m22 = MM[4*ipix_m +3] ; 
    
    float b1,b2;
    b1 =  m11 * a1 + m12 * a2 ;
    b2 =  m21 * a1 + m22 * a2 ;

    d_pos_y[ipix  ] = b1  ;
    d_pos_x[ipix  ] = b2  ;
    
  }
}


__global__ static void  multM_T_kernel( float *d_pos_y, float * d_pos_x,  float *MM ,  int size0, int size1)  {
  // As its non transposed counterpart but, this time, using M transposed. Whence the T postfix
  
  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;
  
  int ix = 16*bidx+tidx ; 
  int iy = 16*bidy+tidy ;
  int ipix = iy*(size1+1)+ix; 
  if( ix< size1+1 && iy < size0+1) {

    int ipix_m = min( iy  , size0 - 1 )*(size1)+min( ix, size1 - 1); 
    
    float a1,a2;
    a1 = d_pos_y[ipix  ];
    a2 = d_pos_x[ipix  ];

    float m11, m12, m21, m22 ; 

    m11 = MM[4*ipix_m +0] ; 
    m12 = MM[4*ipix_m +2] ; 
    m21 = MM[4*ipix_m +1] ; 
    m22 = MM[4*ipix_m +3] ; 
    
    float b1,b2;
    b1 =  m11 * a1 + m12 * a2 ;
    b2 =  m21 * a1 + m22 * a2 ;

    d_pos_y[ipix  ] = b1  ;
    d_pos_x[ipix  ] = b2  ;
    
  }
}

__global__ static void  der_N_central_kernel( float *d_pos_y, float * d_pos_x,  float *scal , int size0, int size1)  {
  // The scalar scal will be derived along the two detector pixel dimension using the central derivative expression.
  // "N" stands for normal
  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;
  
  int ix = 16*bidx+tidx ; 
  int iy = 16*bidy+tidy ;
  if( ix< size1+1 && iy < size0+1) {

    int iy0, iy1, ix0, ix1;

    iy0 = max(0, iy-1);
    iy1 = min( size0-1, iy);
    
    ix0 = max(0, ix-1);
    ix1 = min( size1-1, ix);

    float f00, f10, f01, f11;
    
    f00 = scal[iy0*size1+ix0];
    f10 = scal[iy1*size1+ix0];
    f01 = scal[iy0*size1+ix1];
    f11 = scal[iy1*size1+ix1];
    
    d_pos_y[ iy*(size1+1) + ix] = ( ( f11 + f10 ) - (f01 + f00 )   )*0.5f;
    d_pos_x[ iy*(size1+1) + ix] = ( ( f11 + f01 ) - (f10 + f00 )   )*0.5f;
    
  }
}


__global__ static void  der_T_central_kernel( float *d_pos_y, float * d_pos_x,  float *scal , int size0, int size1)  {
  // as its normal counterpart but this time the transposed operation going from the derivatives to a scalar field.
  //
  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;
  
  int ix = 16*bidx+tidx ; 
  int iy = 16*bidy+tidy ;
  if( ix< size1+1 && iy < size0+1) {

    int iy0, iy1, ix0, ix1;

    iy0 = max(0, iy-1);
    iy1 = min( size0-1, iy);
    
    ix0 = max(0, ix-1);
    ix1 = min( size1-1, ix);

    atomicAdd( &(scal[iy0*size1+ix0]),    0.5f * (-d_pos_y[ iy*(size1+1) + ix] -  d_pos_x[ iy*(size1+1) + ix] )  );
    atomicAdd( &(scal[iy1*size1+ix0]),    0.5f * (+d_pos_y[ iy*(size1+1) + ix] -  d_pos_x[ iy*(size1+1) + ix] )  );
    atomicAdd( &(scal[iy0*size1+ix1]),    0.5f * (-d_pos_y[ iy*(size1+1) + ix] +  d_pos_x[ iy*(size1+1) + ix] )  );
    atomicAdd( &(scal[iy1*size1+ix1]),    0.5f * (+d_pos_y[ iy*(size1+1) + ix] +  d_pos_x[ iy*(size1+1) + ix] )  );
  }
}


__global__ static void    Shrink_kernel ( float * d_target ,
					  float *d_tmp  ,
					  int target_size0,
					  int target_size1,
					  int size0,
					  int size1,
					  int M ,
					  int L  ) {
  // Shrinks the image d_tmp unto d_target by averaging over L x L wide tiles
  // The image are scalar for M=1 or vectorial for M>1
  
  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;

  int ix = 16*bidx+tidx ; 
  int iy = 16*bidy+tidy ;
  int ipix = iy*target_size1+ix; 

  if( ix< target_size1 && iy < target_size0) {
    for(int im=0; im<M; im++) {
      float sum = 0.0f;
      for(int ky = 0; ky<L; ky++) { 
	for(int kx = 0; kx<L; kx++) { 
	  sum +=  d_tmp[  (  ( iy*L+ky )*size1 +  L*ix+kx   )*M+im  ] ;
	}
      }
      d_target[  ipix*M+im ] = sum/L/L ;
    }
  } 
}

__global__ static void    Expand_kernel ( float * d_target ,
						    float *d_tmp  ,
						    int target_size0,
						    int target_size1,
						    int size0,
						    int size1,
						    int M ,
						    int L  ) {
  
  // Expands the image d_tmp unto d_target by a L x L oversampling.
  // The image are scalar for M=1 or vectorial for M>1

  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;

  int ix = 16*bidx+tidx -1;  
  int iy = 16*bidy+tidy -1;
  int ipix = max(0,iy)*size1+max(0,ix); 

  const float fL  = 1.0f/L  ;
  
  if( ix< size1 && iy < size0) {
    {
      for(int im=0; im<M; im++) {
	
	float a00 = d_tmp[  ipix*M+im ];
	float a01 = a00  ; 
	float a10 = a00 ; 
	float a11 = a00 ;
	
	if( ix<size1-1) {
	  a01 = d_tmp[  (  max(0,iy)*size1+max(0,ix+1)    )*M+im ];
	}
	
	if( iy<size0-1) {
	  a10 = d_tmp[  ( max(0,iy+1)*size1+max(0,ix))*M+im ];
	}

	if( ix<size1-1 && iy<size0-1) {
	  a11 = d_tmp[  (max(0,iy+1)*size1+max(0,ix+1))*M+im ];
	}
	for(int ky=0; ky<L; ky++) {
	  for(int kx=0; kx<L; kx++) {	    
	    float truc   = (   (a00 *(1-(kx+0.5f)*fL)    + a01 * ((kx+0.5f)*fL) ) * (1-(ky+0.5f)*fL)
			      +(a10*(1-(kx+0.5f)*fL)    + a11 * ((kx+0.5f)*fL) ) * (  (ky+0.5f)*fL)
			      ) ;
	    int ty;
	    int ii=0;
	    if(ky >= L/2) {
	      ty =  (iy+1)*L+(ky-L/2);
	      ii=2;
	    } else {
	      ty =  (iy)*L+(ky+L/2);
	      ii=0;
	    }
	    
	    int tx;
	    if(kx >= L/2) {
	      tx =  (ix+1)*L+(kx-L/2);
	      ii+=1;
	    } else {
	      tx =  (ix)*L+(kx+L/2);
	    }

	    if( tx>=0 && ty>=0 && tx<target_size1  && ty<target_size0 ) {
	      d_target[  ( ty*target_size1+tx )*M+im  ] =   truc  ;
	    }
	  }
	}
      }
    }
  } 
}


void Rescale_bidirectional(float *d_rescaled, float *h_fixed_scale , int size0, int size1, int M  , float *d_tmp, int direction, int scaling_level_respect_to_fixed ){
  /* For preconditioning purposes the EPR algorithm is runned several times, starting first on a coarse grid to ensure the convergence of the low frequencies, and then
    each time the solutions from the previous step is used as initial guess for the increasingly accurate scales. The solutions are always kept at the original scale,
    the scale of the detector pixel, and stored in h_fixed_scale. Calculations are made with d_rescaled. This routines transform an image at one scale into an image at another scale,
    forth and back according to argument "direction". The images may possibly be composed of derivative at each pixel (M=2). Or matrices (M=4)
  */

  
  float FP=1;

  // account for the denominator rescaling in the derivatives
  if(M==2  ) {
    FP = FP/ pow(2.0,scaling_level_respect_to_fixed) ;
  }
  if( M==4) {
    FP = FP/ pow(4.0,scaling_level_respect_to_fixed) ;        
  }
  
  int rescaled_size0 = size0  ; 
  int rescaled_size1 = size1  ;
  int L=1;
  if(scaling_level_respect_to_fixed>0) {
    for(int i=0; i< scaling_level_respect_to_fixed; i++) {
        rescaled_size0 /= 2  ; 
	rescaled_size1 /= 2  ;
	L*=2;
    }
  } else if(scaling_level_respect_to_fixed<0) {
    for(int i=0; i< -scaling_level_respect_to_fixed; i++) {
        rescaled_size0 *= 2  ; 
	rescaled_size1 *= 2  ;
	L*=2;
    }
  }
  
  int NblocchiY =  iDivUp(  min(size0 , rescaled_size0)+1 , 16 );
  int NblocchiX =  iDivUp(  min(size1 , rescaled_size1)+1 , 16 );
  
  int Npix = size0*size1;
  int rescaledNpix = rescaled_size0*rescaled_size1;


  dim3  dimGrid  ( NblocchiX ,   NblocchiY );
  dim3  dimBlock (  16  ,  16   );

  assert( direction==1 || direction==-1) ;
  
  if(scaling_level_respect_to_fixed==0) {
    if(direction>0) {
      CUDA_SAFE_CALL( cudaMemcpy( d_rescaled, h_fixed_scale  ,  sizeof(float) * ( M*Npix )  , cudaMemcpyHostToDevice) );
    } else if( direction<0) {
      CUDA_SAFE_CALL( cudaMemcpy( h_fixed_scale  ,d_rescaled,   sizeof(float) * ( M*Npix )  , cudaMemcpyDeviceToHost) );
    } 
    return;
  } else {

    if(direction>0) {
    
      if(scaling_level_respect_to_fixed) {
      
	CUDA_SAFE_CALL( cudaMemcpy( d_tmp, h_fixed_scale  ,  sizeof(float) * ( Npix*M )  , cudaMemcpyHostToDevice) );
	
	if(scaling_level_respect_to_fixed>0) {
	  Shrink_kernel<<<dimGrid, dimBlock>>> ( d_rescaled , d_tmp  ,   rescaled_size0, rescaled_size1,  size0, size1, M , L  ) ;
	} else {
	  Expand_kernel<<<dimGrid, dimBlock>>> ( d_rescaled , d_tmp  ,   rescaled_size0, rescaled_size1,  size0, size1, M , L  ) ;
	}

	if(FP!=1.0) {
	  cublasSscal(  rescaledNpix*M , FP, d_rescaled , 1    );
	}

      
      } else {
	CUDA_SAFE_CALL( cudaMemcpy( d_rescaled, h_fixed_scale  ,  sizeof(float) * ( Npix*M )  , cudaMemcpyHostToDevice) );

      }
    } else if(direction<0) {
  
      if(scaling_level_respect_to_fixed) {

	if(scaling_level_respect_to_fixed>0) {
	  Expand_kernel <<<dimGrid, dimBlock>>>  ( d_tmp, d_rescaled ,   size0, size1,  rescaled_size0, rescaled_size1,  M , L  ) ;
	} else {
	  Shrink_kernel<<<dimGrid, dimBlock>>>  ( d_tmp, d_rescaled ,   size0, size1, rescaled_size0, rescaled_size1,  M , L  ) ;
	}
	CUDACHECK ; 
      
	CUDA_SAFE_CALL( cudaMemcpy( h_fixed_scale,  d_tmp,  sizeof(float) * ( Npix*M )  , cudaMemcpyDeviceToHost) );
      
      } else {
	CUDA_SAFE_CALL( cudaMemcpy(  h_fixed_scale  ,d_rescaled,  sizeof(float) * ( Npix*M )  , cudaMemcpyDeviceToHost) );
      }
    } 
  }
}
void Rescale(float *d_target, float *h_source , int size0, int size1, int M  , float *d_tmp, int scaling_level_respect_to_fixed, int direction, int do_twice_for_abs_part=0 );

void Rescale(float *d_target, float *h_source , int size0, int size1, int M  , float *d_tmp, int scaling_level_respect_to_fixed, int direction, int do_twice_for_abs_part ){
  
  Rescale_bidirectional( d_target,  h_source , size0,  size1,  M  , d_tmp, direction , scaling_level_respect_to_fixed);
  
  if (do_twice_for_abs_part) {
    // This is for the variables, they contain the real, and the the imaginary part ( although with
    // a fixed delta/beta ration they are constrained, they are used as for the general case )
    
    int Npix = size0*size1;
    int Npix_rescaled = Npix;
    if ( scaling_level_respect_to_fixed > 0 ) {
      int L = 1 << (scaling_level_respect_to_fixed) ; 
      Npix_rescaled = (  size0  / L ) * (size1  / L ) ; 
    } else if (scaling_level_respect_to_fixed < 0 )  {
      int L = 1 << (-scaling_level_respect_to_fixed) ; 
      Npix_rescaled = (  size0 * L  ) * (size1  * L  ) ; 
    }
    
    Rescale_bidirectional( d_target + Npix_rescaled,  h_source + Npix, size0,  size1,  M  , d_tmp, direction , scaling_level_respect_to_fixed);

  }
  
}





__global__ static void      ElementWiseMultSqrt_kernel(    float *a,
							   float *b,
							   int    Npix,
							   int size0,
							   int size1
  ) {
  // this will be  used to ponderate the error
  //
  int ipix = blockDim.x * blockIdx.x + threadIdx.x ;
  while ( ipix < Npix ) {

    a[ipix] = a[ipix]/sqrtf(b[ipix]) ;
    ipix += blockDim.x * gridDim.x ;
  }
}

void ElementWiseMultSqrt(  float *a , float *b ,  int N , int size0, int size1  ) {
  // this will be  used to ponderate the error
  //
  int NblocksPerGrid=  min( 0xFFFF , (N+512-1) / 512 );
  ElementWiseMultSqrt_kernel<<<NblocksPerGrid, 512>>> ( a,b, N, size0,  size1 );
}


cuErrGradArgs * gpu_Proj_calcGrad_ArgsConvert( AspectErrGradArgs * Args  , int scale_level_respect_to_expanded) {
  // the task performed by this routine consists in providing an object of type cuErrGradArgs which 
  // -- corresponds to a given scaling level
  // -- contains variables, parameters, and data already allocated for the GPU

  
  float * d_Q2use = NULL;
  float * d_phase_and_logT_buffer = NULL;

  float * d_radio_data_ovs ;
  float * d_flat_field ;
  float * d_mGrad  ; 
  float * d_pos_y  ;
  float * d_pos_x  ;
  float * der_pos_y  ;
  float * der_pos_x  ;
  float * d_tmp_laplacian  ;
  float * d_P_gc ;
  float * d_M_phase2shift  ;
  float * d_Field_intensity  ;
  double * d_auxiliary_for_reduction ; 
  double * d_one ;
  float * d_mGrad_old  ; 
  float * d_dmGda_along_p  ; 
  float *d_BUFFER ;  
  float *  d_Tmp_For_auxiliary_expansion;
  float *  d_spettro_data;


  // si potra levare lo static
  
  cuErrGradArgs *cuArgs = new cuErrGradArgs; 
  AspectSetup   *myexp_setup = new AspectSetup; 
    
  *myexp_setup = *(Args->exp_setup);

  int OVS = myexp_setup->OVS ;
  int OVS2 = OVS * OVS ;
  float OVS_F = 1.0 / OVS ; 
  
  cuArgs->exp_setup = myexp_setup ; 
  
  myexp_setup->size0 = (1+AUXILIARY_EXPANSION)*Args->exp_setup->size0 ; 
  myexp_setup->size1 = (1+AUXILIARY_EXPANSION)*Args->exp_setup->size1 ; 
  cuArgs->Npix =  myexp_setup->size0* myexp_setup->size1  ;
  cuArgs->n_spettro = Args->n_spettro;
  
  cuArgs->errore = Args->errore ;
  
  //  if( d_Q2use == NULL ) {
  {
    
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_Q2use),  sizeof(float)*  (cuArgs->Npix  +  cuArgs->Npix)  ) );
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_spettro_data),  sizeof(float)*  (4*cuArgs->n_spettro  ) ) );
    cudaMemcpy( d_spettro_data  , Args->spettro_data  , sizeof(float)* (4*cuArgs->n_spettro  ) , cudaMemcpyHostToDevice);

		   
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_phase_and_logT_buffer),  sizeof(float)*  (cuArgs->Npix  +  cuArgs->Npix)  ) );
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_P_gc),  sizeof(float)*  (cuArgs->Npix  +  cuArgs->Npix)  ) );
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_mGrad),  sizeof(float)*  (cuArgs->Npix  +  cuArgs->Npix)  ) );

    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_pos_y),   sizeof(float)*  (cuArgs->Npix )*2  ) );
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_pos_x),   sizeof(float)*  (cuArgs->Npix )*2  ) );
    
    CUDA_SAFE_CALL(cudaMalloc((void **)&(der_pos_y),   sizeof(float)*  (cuArgs->Npix )*2  ) );
    CUDA_SAFE_CALL(cudaMalloc((void **)&(der_pos_x),   sizeof(float)*  (cuArgs->Npix )*2  ) );

    
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_tmp_laplacian),  sizeof(float)*  ((cuArgs->Npix )*2   ) ) ); 
    
    
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_radio_data_ovs     )  ,  cuArgs->Npix * OVS2 * sizeof(float) ) ) ;
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_flat_field      )  ,  cuArgs->Npix * sizeof(float) ) ) ;
    


    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_BUFFER  )  ,  cuArgs->Npix * OVS2* sizeof(float) ) ) ;
    
    
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_M_phase2shift),  sizeof(float)*  (cuArgs->Npix )*4  ) );
    
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_Field_intensity),   sizeof(float)*  (cuArgs->Npix )  ) );
    
    
    {
      int max_NblocksPerGrid=  min( 0xFFFF , (cuArgs->Npix+cuArgs->Npix+512-1) / 512 );
      
      CUDA_SAFE_CALL(cudaMalloc((void **)&(d_auxiliary_for_reduction),   sizeof(double)*  ( max_NblocksPerGrid  )  ) );
    }
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_one)      ,   sizeof(double)*  (1              )  ) );
    {
      double one = 1;
      cudaMemcpy(  d_one , &one , sizeof(double)*1 , cudaMemcpyHostToDevice);
    }
    
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_mGrad_old),  sizeof(float)*  (cuArgs->Npix  +  cuArgs->Npix)  ) );
    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_dmGda_along_p),  sizeof(float)*  (cuArgs->Npix  +  cuArgs->Npix)  ) );

    CUDA_SAFE_CALL(cudaMalloc((void **)&(d_Tmp_For_auxiliary_expansion),  sizeof(float)*  ( 1+ 4*         myexp_setup->size0* myexp_setup->size1  )  ) );
    
  }

  // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


  if(scale_level_respect_to_expanded) {  // scale_level_respect_to_expanded e' rispetto a estensione massima auxiliary_expansion compreso
    int p2=1;
    for(int i=0; i<scale_level_respect_to_expanded; i++) p2*=2;
    
    myexp_setup->size0 = (1+AUXILIARY_EXPANSION)*Args->exp_setup->size0 /p2; 
    myexp_setup->size1 = (1+AUXILIARY_EXPANSION)*Args->exp_setup->size1 /p2; 
    cuArgs->Npix = myexp_setup->size0 * myexp_setup->size1  ;
    
  }

  {
    
    int scale_level_respect_to_fixed = scale_level_respect_to_expanded;    // scale_level_respect_to_fixed e' rispetto a estensione naturale senza auxiliary_expansion
    if(AUXILIARY_EXPANSION) scale_level_respect_to_fixed=scale_level_respect_to_fixed-1;

    
    Rescale(  d_M_phase2shift   ,  Args->exp_setup->M_phase2shift ,   Args->exp_setup->size0, Args->exp_setup->size1, 4  , d_Tmp_For_auxiliary_expansion , scale_level_respect_to_fixed, RESCALE_FORWARD );


    {

      int Npix_orig = Args->exp_setup->size0* Args->exp_setup->size1  ; 
      float *tmp_qi =  (float *) malloc(  2* Npix_orig  * sizeof(float) ) ; 

      for(int i=0; i< Npix_orig; i++) {
	
	tmp_qi[Npix_orig+i] = log(  Args->transm[i]);
	tmp_qi[i] = tmp_qi[Npix_orig+i] *    Args->exp_setup->DBR_half / Args->exp_setup->DB_EQUILIBRATION_FACTOR  ;
	
      }
      Rescale(  d_Q2use,  tmp_qi,   Args->exp_setup->size0, Args->exp_setup->size1, 1,  d_Tmp_For_auxiliary_expansion, scale_level_respect_to_fixed, RESCALE_FORWARD, REPLICATE_FOR_ABS );

      
      free(tmp_qi);

    }
       
    Rescale(  d_radio_data_ovs , Args->radio_data  ,   Args->exp_setup->size0, Args->exp_setup->size1, 1  , d_Tmp_For_auxiliary_expansion, scale_level_respect_to_fixed-(OVS-1), RESCALE_FORWARD);
    
    if (OVS !=1) {
      cublasSscal(  OVS2 * Args->exp_setup->size0 * Args->exp_setup->size1   , OVS_F * OVS_F, d_radio_data_ovs , 1  );
    }

    Rescale(    d_flat_field    ,  Args->flat_field  ,   Args->exp_setup->size0, Args->exp_setup->size1, 1  , d_Tmp_For_auxiliary_expansion, scale_level_respect_to_fixed, RESCALE_FORWARD);


  }
  
  cuArgs->d_phase_and_logT = d_Q2use ;
  
  cuArgs->d_phase_and_logT_buffer = d_phase_and_logT_buffer ; 

  cuArgs->d_radio_data_ovs = d_radio_data_ovs ; 

  cuArgs->d_flat_field = d_flat_field ; 
  cuArgs->d_spettro_data = d_spettro_data ; 
  cuArgs->d_mGrad = d_mGrad ; 

  cuArgs->d_mGrad_old = d_mGrad_old ; 
  cuArgs->dmGda_along_p =  d_dmGda_along_p ; 

  cuArgs->d_pos_y = d_pos_y ;
  cuArgs->d_pos_x = d_pos_x ;
  
  cuArgs->der_pos_y = der_pos_y ;
  cuArgs->der_pos_x = der_pos_x ;
  
  cuArgs->d_tmp_laplacian = d_tmp_laplacian ; 

  cuArgs->d_P_gc = d_P_gc ; 
  cuArgs->d_M_phase2shift = d_M_phase2shift ; 
  cuArgs->d_Field_intensity = d_Field_intensity ;

  cuArgs->d_auxiliary_for_reduction = d_auxiliary_for_reduction ; 
  cuArgs->d_one = d_one ; 
      
  cuArgs->d_BUFFER       =  d_BUFFER ; 

  cuArgs->d_Tmp_For_auxiliary_expansion = d_Tmp_For_auxiliary_expansion; 
  
  return cuArgs;
}

void free_allocations(cuErrGradArgs *cuArgs) {
  
  cudaFree( cuArgs->d_phase_and_logT);
  cudaFree( cuArgs->d_phase_and_logT_buffer );

  cudaFree( cuArgs->d_radio_data_ovs   );
  cudaFree( cuArgs->d_flat_field  );
  cudaFree( cuArgs-> d_mGrad  );
  cudaFree( cuArgs->d_mGrad_old  );
  cudaFree( cuArgs->d_pos_y  );
  cudaFree( cuArgs->d_pos_x  );
  
  cudaFree( cuArgs->der_pos_y  );
  cudaFree( cuArgs->der_pos_x  );
  
  cudaFree( cuArgs->d_tmp_laplacian  );
  cudaFree( cuArgs->d_P_gc  );
  cudaFree( cuArgs->d_M_phase2shift   );
  cudaFree( cuArgs->d_Field_intensity  );
  cudaFree( cuArgs->d_auxiliary_for_reduction   );
  cudaFree( cuArgs->d_one  );
  
  cudaFree( cuArgs->d_BUFFER );
  cudaFree( cuArgs->d_Tmp_For_auxiliary_expansion  );
  cudaFree( cuArgs-> dmGda_along_p );
  cudaFree( cuArgs-> d_spettro_data );

  delete cuArgs->exp_setup ;
  delete cuArgs ; 
  
	
}


__global__ static void     projectGrad_kernel(    float *d_mGrad   ,
						  int Npix,
						  float vr,
						  float vi
						  ) {
  // with the purpose of keeping the gradient parallel to a constant delta/beta plane
  // this kernel projects the gradient on such plane
  
  int ipix = blockDim.x * blockIdx.x + threadIdx.x ;
  float pr, pi , tmp;
  
  while ( ipix < Npix ) {
        
    pr = d_mGrad[       ipix];
    pi = d_mGrad[Npix+ipix];
    
    tmp = pr*vr+pi*vi ;
    
    pr = tmp * vr ; 
    pi = tmp * vi ; 
    
    d_mGrad[       ipix] = pr;
    d_mGrad[Npix+ipix] = pi;
    
    ipix += blockDim.x * gridDim.x ;
  }
}

__global__ static void      scalD_kernel(    float *a,
					     float *b,
					     double*d_auxiliary_for_reduction ,
					     int    Npix
									     ) {
  //  a kernel for the scalar product of two vectors of float with accumulation on double precision result
  //
  
  volatile __shared__ double chache[512] ;
  int ipix = blockDim.x * blockIdx.x + threadIdx.x ;
  const unsigned int tid = threadIdx.x ;
  double result = 0.0 ; 
  while ( ipix < Npix ) {
    // result += 0.5*a[ipix]*b[ipix];
    result += a[ipix]*b[ipix];
    ipix += blockDim.x * gridDim.x ;
  }
  chache[tid] = result ;
  __syncthreads () ;
  
  {  if(tid<256) { chache[tid] += chache [tid + 256]; } __syncthreads();  }
  {  if(tid<128) { chache[tid] += chache [tid + 128]; } __syncthreads();  }
  {  if(tid<64) { chache[tid]  += chache [tid + 64]; } __syncthreads();  }
  if(tid<32) {
    chache[tid]  += chache [tid + 32];
    chache[tid]  += chache [tid + 16];
    chache[tid]  += chache [tid + 8];
    chache[tid]  += chache [tid + 4];
    chache[tid]  += chache [tid + 2];
    chache[tid]  += chache [tid + 1];
  }
  if ( tid == 0 ) {
    d_auxiliary_for_reduction[blockIdx.x] = chache [0] ;
  }
}


double scalD( cuErrGradArgs * CA,  float *a , float *b ,  int N   ) {
  //  a function for the scalar product of two vectors of float with accumulation on double precision result 

  int NblocksPerGrid=  min( 0xFFFF , (N+512-1) / 512 );
  scalD_kernel<<<NblocksPerGrid, 512>>> ( a,b,CA->d_auxiliary_for_reduction, N  );
  return  cublasDdot(   NblocksPerGrid  , CA->d_one , 0, CA->d_auxiliary_for_reduction , 1) ;
}



#define TILE_SIZE 8
// this hard coded parameter is the size of a tile considered in the kernel for projecting the intensity on the detector





 __device__ float  integral_over_pixel( float a_x, float a_y, float b_x, float b_y, float &fstart, float &fend, float *path_x, float *path_y) {
   // this kernel calculates the area of a pixel which is found under or above (with sign accounting) a given segment 

  
   path_x[0] = a_x; path_x[1] = a_x;  path_x[2] = b_x;   path_x[3] = b_x; 
   path_y[0] = a_y; path_y[1] = a_y;  path_y[2] = b_y;   path_y[3] = b_y;

   float fx_left, fx_right, fy_down, fy_up ;

   float step_x =  b_x - a_x ; 
   if ( step_x !=0 ) { // sempre maggiore o uguale
     fx_left  = ( -0.5f - a_x)/ step_x ;
     fx_right = ( +0.5f - a_x)/ step_x ;
   } else {
     fx_left  = - signf_copy (step_x,   INFINITY) ; 
     fx_right = + signf_copy (step_x,   INFINITY)  ; 
   }

   float step_y =  b_y - a_y ; 
   if ( step_y !=0 ) { 
     fy_down  = ( -0.5f - a_y)/ step_y ;
     fy_up    = ( +0.5f - a_y)/ step_y ;
   } else {
     fy_down  = - signf_copy (step_y,   INFINITY) ; 
     fy_up    = + signf_copy (step_y,   INFINITY)  ; 
   }

   fstart = fmaxf( fmaxf(0, fx_left), fminf( fy_down,  fy_up  ) );
   fend   = fminf( fminf(1, fx_right), fmaxf( fy_down,  fy_up  ) );

   if( fstart >=0.0f && fstart <= 1.0f ) {
     float my_x  =   a_x + fstart *( b_x -a_x )   ; 
     float my_h  =   a_y + fstart *( b_y -a_y )   ; 

     path_x[0] = max(-0.5, a_x )  ; path_x[1] = my_x;  
     path_y[0] =  my_h ; path_y[1] = my_h;      
   }
   if( fend >=0.0f && fend <= 1.0f ) {
     float my_x  =   a_x + fend *( b_x -a_x )   ; 
     float my_h  =   a_y + fend *( b_y -a_y )   ; 

     path_x[2] = my_x  ; path_x[3] = min(+0.5f, b_x);  
     path_y[2] =  my_h ; path_y[3] = my_h;      
   }
   for(int i=0; i<4; i++) {
     path_y[i] = clamp( path_y[i] , -0.5f, 0.5f);
     path_x[i] = clamp( path_x[i] , -0.5f, 0.5f);
   }

   float res = 0.0f;
   for(int i=0; i<3; i++) {
     res +=  ( path_y[i] + path_y[i+1] )*( path_x[i+1] - path_x[i] ) * 0.5f ;
   }
   return res;
 }


 __device__  void add_to_area_der( float ax, float ay, float bx, float by,  float segno, float fstart_arg, float fend_arg, float &dt_dax, float &dt_day, float &dt_dbx, float &dt_dby, float * path_x,float * path_y ) {
   // this kernel calculates the derivatives, with respect to segment coordinates, of the area of a pixel which is found under or above (with sign accounting) a given segment 
   
   
   if( fend_arg>fstart_arg) {

     float f_step = ( fend_arg - fstart_arg ) ;
     float dx  = (bx-ax) *f_step ;
     float dy  = (by-ay) *f_step ;

     float f_middle = ( fstart_arg+fend_arg)*0.5f;
     if(segno<0) {
       f_middle = 1-f_middle;
     }
     
     dt_dax +=   -dy * ( 1 - f_middle )  ; 
     dt_day +=   +dx * ( 1 - f_middle )  ;
     dt_dbx +=   -dy * (     f_middle )  ; 
     dt_dby +=   +dx * (     f_middle )  ; 
   }

   int isegno = int(segno);
   int i0 = 3*(1-isegno)/2;
   int i1 = i0 + isegno   ;
   int i3 = 3*(1+isegno)/2;
   int i2 = i3 - isegno   ;
   
   if( path_x[i0] > -0.5f &&  path_x[i0] <0.5f) {
     dt_dax -= path_y[i1] ;
   }
   
   if(  path_x[i3] > -0.5f && path_x[i3] < +0.5f ) {
     dt_dbx += path_y[i2];
   }   
 }



// #   d1 > d2
#define MIND0( a)   ( (a)>0?  (a)*d2:(a)*d1    )
#define MIND1( a)   ( (a-1)>0?  (1+(a-1)*d2):(1+(a-1)*d1) )
#define MAXD0( a)   ( (a)>0?  (a)*d1:(a)*d2    )
#define MAXD1( a)   ( (a-1)>0?  (1+(a-1)*d1):(1+(a-1)*d2) )
#define DILAT0(a)  ((a)*dila)
#define DILAT1(a)  ((a-1)*dila+1)

__global__ static void    fillBUFFER_kernel(
							   float *d_BUFFER,
							   int size0,
							   int size1,

							   float *flat_field ,
							   float *d_Field_intensity,

							   float *d_pos_y ,
							   float *d_pos_x ,
							   int n_spettro,
							   float *spettro_data,
							   int ovs,
							   int spectral_binning
							   ) {
  // this kernel fills a buffer image with the projected intensity
  
#include"spettro.h"
  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;
  const int Dx = blockDim.x;
  const int Dy = blockDim.y;
  
  int iy_s =  (bidy*Dy + tidy)   ;
  int ix_s =  (bidx*Dx + tidx)   ;

  int ipix_s = ix_s + iy_s*size1;

  int OVS = ovs ;

  float OVS_F = 1.0 / OVS ; 
  
  float path_x[4];
  float path_y[4];
  
  float d1 = spettro_data[0*4 + delta_ratio_pos];
  float d2 = spettro_data[4*(n_spettro-1) + delta_ratio_pos];
  
  if( iy_s  < size0 && ix_s < size1) {
    
    float p_s00_y =        +  d_pos_y[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s10_y = + 1.0f +  d_pos_y[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s01_y =        +  d_pos_y[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 1 ) ] ;  
    float p_s11_y = + 1.0f +  d_pos_y[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 1 ) ] ;
    
    float min_y , max_y ; 
    
    min_y = iy_s +  min(  MIND0(p_s00_y),  min(  MIND1(p_s10_y), min( MIND0(p_s01_y),  MIND1(p_s11_y) ) ) ) ;
    max_y = iy_s +  max(  MAXD0(p_s00_y),  max(  MAXD1(p_s10_y), max (MAXD0(p_s01_y),  MAXD1(p_s11_y) ) ) ) ;
    
    int first_y, last_y ; 
    
    first_y = fmaxf( 0      , floor( (min_y - 0.5f)*OVS ));
    last_y  = fminf( OVS * size0-1, ceil ( (max_y + 0.5f)*OVS ));
    
    float p_s00_x =        +  d_pos_x[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s10_x =        +  d_pos_x[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s01_x = + 1.0f +  d_pos_x[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 1 ) ] ;  
    float p_s11_x = + 1.0f +  d_pos_x[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 1 ) ] ;
    
    float min_x , max_x ; 

    
    min_x = ix_s + fminf(  MIND0(p_s00_x),  fminf(MIND0(p_s10_x), fminf(MIND1(p_s01_x),  MIND1(p_s11_x) ) ) );
    max_x = ix_s + fmaxf(  MAXD0(p_s00_x),  fmaxf(MAXD0(p_s10_x), fmaxf(MAXD1(p_s01_x),  MAXD1(p_s11_x) ) ) );
    
    int first_x, last_x ; 
    
    first_x = fmaxf( 0.0f    , floor( (min_x - 0.5f)*OVS ));
    last_x  = fminf( OVS * size1-1 , ceil ( (max_x + 0.5f)*OVS ));
    int n_loops_x = ( last_x +1 - first_x ) ; 
    int n_loops_y = ( last_y +1 - first_y ) ;
    
    int total_loops =  n_loops_y * n_loops_x ;

    for(int triangle = 0 ; triangle < 2 ; triangle++ )  { 

      int aggregate= 1;
      float delta_ratio =0 ;  
      float my_Field_intensity=0  ;
        
      for(int i_spectra = 0; i_spectra < n_spettro; i_spectra++) {


	if ( aggregate==0 ) {
	  float ene_ratio = spettro_data[i_spectra*4+ene_ratio_pos] ;  
	  float beta_ratio = spettro_data[i_spectra*4+beta_ratio_pos] ;  
	  delta_ratio = spettro_data[i_spectra*4+delta_ratio_pos] ;  
	  float weig = spettro_data[i_spectra*4+fraction_pos] ;  	
	  my_Field_intensity  = __expf(__logf(d_Field_intensity[ipix_s])*beta_ratio*ene_ratio ) * weig ;
	} else {
	  float ene_ratio = spettro_data[i_spectra*4+ene_ratio_pos] ;  
	  float beta_ratio = spettro_data[i_spectra*4+beta_ratio_pos] ;  
	  float new_delta_ratio = spettro_data[i_spectra*4+delta_ratio_pos] ;  
	  float weig = spettro_data[i_spectra*4+fraction_pos] ;  	
	  float new_my_Field_intensity  = __expf(__logf(d_Field_intensity[ipix_s])*beta_ratio*ene_ratio ) * weig ;

	  delta_ratio = ( my_Field_intensity * delta_ratio +   new_my_Field_intensity *   new_delta_ratio    ) / (my_Field_intensity + new_my_Field_intensity  );
	  my_Field_intensity = (my_Field_intensity + new_my_Field_intensity  );
	}

	float contribution = 0;
      	float dila = delta_ratio;

	
	float dil_p00_y =  DILAT0( p_s00_y ) ; // la rete dei centri derivata é spostata di mezzo pixel  
	float dil_p10_y =  DILAT1( p_s10_y ) ;  
	float dil_p01_y =  DILAT0( p_s01_y )  ;  
	float dil_p11_y =  DILAT1( p_s11_y )  ;  
	    
	float dil_p00_x =  DILAT0( p_s00_x ) ;  
	float dil_p10_x =  DILAT0( p_s10_x ) ;  
	float dil_p01_x =  DILAT1( p_s01_x ) ;  
	float dil_p11_x =  DILAT1( p_s11_x ) ;  

	if( aggregate==0 ) {
          // non serve tutto con period
	  float tol=0.5;
	  if(  fabs(dil_p00_y    ) > tol ||  fabs(dil_p01_y     ) > tol ||  fabs(dil_p00_x     ) > tol ||  fabs(dil_p10_x     )>tol ||
	       fabs(dil_p11_y - 1) > tol ||  fabs(dil_p10_y - 1 ) > tol ||  fabs(dil_p11_x - 1 ) > tol ||  fabs(dil_p01_x - 1 )>tol 
	       ) {
	  } else {
	    aggregate = 0 ; 
	  }
	}
	  
	if(  aggregate && (i_spectra!=n_spettro-1    && !(i_spectra && i_spectra% spectral_binning ==0 )) ) {
	  continue;
	}
	float ax, ay, bx, by, cx, cy;

	ax = dil_p00_x ;
	ay = dil_p00_y ;
	bx = dil_p11_x ;
	by = dil_p11_y ;	
	if( triangle==0) {
	  cx = dil_p01_x  ;
	  cy = dil_p01_y  ; 
	} else {
	  cx = dil_p10_x  ;
	  cy = dil_p10_y  ; 
	}
	
	float x1 = bx - ax;
	float y1 = by - ay;	
	float x2 = cx - ax;
	float y2 = cy - ay;
	
	float original_triangle_area_signed =  -(x1*y2 -x2*y1)*0.5f  ; 
	float original_triangle_area        = fmaxf(fabs(original_triangle_area_signed ), 0.0000001) ;
	
	for(int loop=0; loop < total_loops ; loop++) {
	  
	  int iy = first_y + loop / n_loops_x;
	  int ix = first_x + loop % n_loops_x;
	  
	  float dy = ( iy_s - iy * OVS_F ) - 0.5f ; 
	  float dx = ( ix_s - ix * OVS_F ) - 0.5f ; 
	  
	  float p00_y = dy + dil_p00_y  ; // la rete dei centri derivata é spostata di mezzo pixel  
	  float p10_y = dy + dil_p10_y  ;  
	  float p01_y = dy + dil_p01_y   ;  
	  float p11_y = dy + dil_p11_y   ;  
	  
	  float p00_x = dx +  dil_p00_x  ;  
	  float p10_x = dx +  dil_p10_x  ;  
	  float p01_x = dx +  dil_p01_x  ;  
	  float p11_x = dx +  dil_p11_x  ;  
 
	  if(  p00_x <= -0.5 * OVS_F && p10_x <= -0.5 * OVS_F && p01_x <= -0.5 * OVS_F  &&  p11_x <= -0.5 * OVS_F ) {
	    continue;
	  }
	  
	  if(  p00_y <= -0.5 * OVS_F && p10_y <= -0.5 * OVS_F && p01_y <= -0.5 * OVS_F  &&  p11_y <= -0.5 * OVS_F ) {
	    continue;
	  }
	  
	  if(  p00_x >= +0.5 * OVS_F && p10_x >= +0.5 * OVS_F && p01_x >= +0.5 * OVS_F  &&  p11_x >= +0.5 * OVS_F ) {
	    continue;
	  }
	  
	  if(  p00_y >= +0.5 * OVS_F && p10_y >= +0.5 * OVS_F && p01_y >= +0.5 * OVS_F  &&  p11_y >= +0.5 * OVS_F ) {
	    continue;
	  }

	  ax = p00_x - (dx ); // removed here, readded later when integral_over_pixel is called. This just to keep the same writing as in usebuffer
	  ay = p00_y - (dy );
	  bx = p11_x - (dx );
	  by = p11_y - (dy );	
	  if( triangle==0) {
	    cx = p01_x  - (dx );
	    cy = p01_y  - (dy ); 
	  } else {
	    cx = p10_x  - (dx );
	    cy = p10_y  - (dy ); 
	  }
	  
	  float area_triangle = 0.0f;
	  float fstart, fend;
	  
	  // 00-11
	  float args[4] ;
	  float segno;
	  if( ax < bx ) {
	    args[0] = ax ; args[1] = ay ; args[2] = bx  ; args[3] = by ;
	    segno = 1.0f ; 
	  } else {
	    args[2] = ax ; args[3] = ay  ; args[0] = bx ; args[1] = by ;
	    segno = -1.0f ; 
	  }
	  
	  area_triangle +=  segno*integral_over_pixel( (args[0] +dx)*OVS, (args[1] +dy)*OVS, (args[2] +dx)*OVS, (args[3] +dy)*OVS, fstart, fend, path_x, path_y)   ;
	  
	  // 00-11
	  if( bx < cx ) {
	    args[0] = bx ; args[1] = by ; args[2] = cx ; args[3] = cy ;
	    segno = 1.0f;
	  } else {
	    args[2] = bx ; args[3] = by ; args[0] = cx ; args[1] = cy ;
	    segno = -1.0f;
	  }
	  
	  area_triangle +=  segno *integral_over_pixel( (args[0] +dx)*OVS, (args[1] +dy)*OVS, (args[2] +dx)*OVS, (args[3] +dy)*OVS, fstart, fend, path_x, path_y)   ;
	  
	  if( cx < ax ) {
	    args[0] = cx ; args[1] = cy ; args[2] = ax ; args[3] = ay ;
	    segno = 1.0f;
	  } else {
	    args[2] = cx ; args[3] = cy ; args[0] = ax ; args[1] = ay ;
	    segno = -1.0f;
	  }	  
	  area_triangle +=  segno *integral_over_pixel( (args[0] +dx)*OVS, (args[1] +dy)*OVS, (args[2] +dx)*OVS, (args[3] +dy)*OVS, fstart, fend, path_x, path_y)   ;
	  
	  contribution =    0.5f* fabs(area_triangle * OVS_F * OVS_F) / original_triangle_area;
	  
	  if ( contribution !=0.0) {
	  
	    int ipix = ix + iy*size1 * OVS ;

	    float tmp =  contribution *  flat_field [ipix_s]  * my_Field_intensity;
	  
	    atomicAdd( d_BUFFER+ipix ,  tmp   );
	  }

	  
	}

	delta_ratio =0 ;  
	my_Field_intensity=0  ;

	
      }
    }
  }
}

__global__ static void    useBUFFER_kernel(
						    float *d_BUFFER,
						    int size0,
						    int size1,

						    float *flat_field ,
						    float *d_Field_intensity,

						    float *d_pos_y ,
						    float *d_pos_x ,

						    float *der_Field_intensity_of_log,
						    float *der_pos_y,
						    float *der_pos_x ,
						    int n_spettro,
						    float *spettro_data,
						    int ovs,
						    int spectral_binning
						    ) {
  
  // this kernel uses the residual error found in buffer image to calculate the gradients

  
#include"spettro.h"
  
  const int tidx = threadIdx.x;
  const int bidx = blockIdx.x;
  const int tidy = threadIdx.y;
  const int bidy = blockIdx.y;
  const int Dx = blockDim.x;
  const int Dy = blockDim.y;


  int OVS = ovs ;

  float OVS_F = 1.0 / OVS ; 
  
  int iy_s =  (bidy*Dy + tidy)   ;
  int ix_s =  (bidx*Dx + tidx)   ;

  int ipix_s = ix_s + iy_s*size1;


  
  float path_x[4];
  float path_y[4];

  float d1 = spettro_data[0*4 + delta_ratio_pos];
  float d2 = spettro_data[4*(n_spettro-1) + delta_ratio_pos];
  
  if( iy_s  < size0 && ix_s < size1) {
    
    float p_s00_y =        +  d_pos_y[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s10_y = + 1.0f +  d_pos_y[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s01_y =        +  d_pos_y[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 1 ) ] ;  
    float p_s11_y = + 1.0f +  d_pos_y[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 1 ) ] ;
    
    float min_y , max_y ; 

    min_y = iy_s +  min(  MIND0(p_s00_y),  min(  MIND1(p_s10_y), min( MIND0(p_s01_y),  MIND1(p_s11_y) ) ) ) ;
    max_y = iy_s +  max(  MAXD0(p_s00_y),  max(  MAXD1(p_s10_y), max (MAXD0(p_s01_y),  MAXD1(p_s11_y) ) ) ) ;

    int first_y, last_y ; 
     
    first_y = fmaxf( 0.0f      , floor( (min_y - 0.5f)*OVS ));
    last_y  = fminf( OVS * size0-1, ceil ( (max_y + 0.5f)*OVS ));

    // p_s position shift
    float p_s00_x =        +  d_pos_x[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s10_x =        +  d_pos_x[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 0 ) ] ;  
    float p_s01_x = + 1.0f +  d_pos_x[  (iy_s + 0) * ( size1 +1 ) + (ix_s + 1 ) ] ;  
    float p_s11_x = + 1.0f +  d_pos_x[  (iy_s + 1) * ( size1 +1 ) + (ix_s + 1 ) ] ;
    
    float min_x , max_x ; 
    
    min_x = ix_s + fminf(  MIND0(p_s00_x),  fminf(MIND0(p_s10_x), fminf(MIND1(p_s01_x),  MIND1(p_s11_x) ) ) );
    max_x = ix_s + fmaxf(  MAXD0(p_s00_x),  fmaxf(MAXD0(p_s10_x), fmaxf(MAXD1(p_s01_x),  MAXD1(p_s11_x) ) ) );
    
    int first_x, last_x ; 
    
    first_x = fmaxf( 0.0f    , floor( (min_x - 0.5f)*OVS ));
    last_x  = fminf( OVS * size1-1 , ceil ( (max_x + 0.5f)*OVS ));
    
    int n_loops_y =  ( last_y +1 - first_y ) ;
    int n_loops_x =  ( last_x +1 - first_x ) ; 
    int total_loops =  n_loops_y * n_loops_x ; 

    for(int triangle = 0 ; triangle < 2 ; triangle++ )  {

      int aggregate= 1;
      float delta_ratio=0  ;  
      float my_Field_intensity=0  ;
      float exp_deri_factor=0 ; 

      
      for(int i_spectra = 0; i_spectra < n_spettro; i_spectra++) {



	if ( aggregate==0 ) {
	  float ene_ratio = spettro_data[i_spectra*4+ene_ratio_pos] ;  
	  float beta_ratio = spettro_data[i_spectra*4+beta_ratio_pos] ;  
	  delta_ratio = spettro_data[i_spectra*4+delta_ratio_pos] ;  
	  float weig = spettro_data[i_spectra*4+fraction_pos] ;  	
	  my_Field_intensity  = __expf(__logf(d_Field_intensity[ipix_s])*beta_ratio*ene_ratio ) * weig ;
	  exp_deri_factor = beta_ratio * ene_ratio  ;
	  
	} else { 
	  float ene_ratio = spettro_data[i_spectra*4+ene_ratio_pos] ;  
	  float beta_ratio = spettro_data[i_spectra*4+beta_ratio_pos] ;  
	  float new_delta_ratio = spettro_data[i_spectra*4+delta_ratio_pos] ;
	  
	  float weig = spettro_data[i_spectra*4+fraction_pos] ;  	
	  float new_my_Field_intensity  = __expf(__logf(d_Field_intensity[ipix_s])*beta_ratio*ene_ratio ) * weig ;
	  float new_exp_deri_factor = beta_ratio*ene_ratio     ;
	  
	  delta_ratio = ( my_Field_intensity * delta_ratio +   new_my_Field_intensity *   new_delta_ratio    ) / (my_Field_intensity + new_my_Field_intensity  );
	  exp_deri_factor  = (  exp_deri_factor *  my_Field_intensity  +    new_exp_deri_factor *  new_my_Field_intensity         )/  (my_Field_intensity + new_my_Field_intensity  ); 
	  my_Field_intensity = (my_Field_intensity + new_my_Field_intensity  );
	 
	}

	
      	float dila = delta_ratio;


	float dil_p00_y =  DILAT0( p_s00_y ) ; // la rete dei centri derivata é spostata di mezzo pixel  
	float dil_p10_y =  DILAT1( p_s10_y ) ;  
	float dil_p01_y =  DILAT0( p_s01_y )  ;  
	float dil_p11_y =  DILAT1( p_s11_y )  ;  
	    
	float dil_p00_x =  DILAT0( p_s00_x ) ;  
	float dil_p10_x =  DILAT0( p_s10_x ) ;  
	float dil_p01_x =  DILAT1( p_s01_x ) ;  
	float dil_p11_x =  DILAT1( p_s11_x ) ;  

	if( aggregate==0 ) {
	  float tol=0.5;
	  if(  fabs(dil_p00_y    ) > tol ||  fabs(dil_p01_y     ) > tol ||  fabs(dil_p00_x     ) > tol ||  fabs(dil_p10_x     )>tol ||
	       fabs(dil_p11_y - 1) > tol ||  fabs(dil_p10_y - 1 ) > tol ||  fabs(dil_p11_x - 1 ) > tol ||  fabs(dil_p01_x - 1 )>tol 
	       ) {
	  } else {
	    aggregate = 0 ;
	  }
	}
	if(  aggregate && (i_spectra!=n_spettro-1    &&  !(  i_spectra && i_spectra % spectral_binning ==0 )) ) {
	  continue;
	}

	
	float ax, ay, bx, by, cx, cy;

	ax = dil_p00_x ;
	ay = dil_p00_y ;
	bx = dil_p11_x ;
	by = dil_p11_y ;	
	if( triangle==0) {
	  cx = dil_p01_x  ;
	  cy = dil_p01_y  ; 
	} else {
	  cx = dil_p10_x  ;
	  cy = dil_p10_y  ; 
	}
	
	float x1 = bx - ax;
	float y1 = by - ay;
	float x2 = cx - ax;
	float y2 = cy - ay;
	
	float original_triangle_area_signed =  -(x1*y2 -x2*y1)*0.5f  ; 
	float original_triangle_area        = fmaxf(fabs(original_triangle_area_signed ), 0.0000001) ;
	
	float sign_over_two = signf_copy(original_triangle_area_signed, 0.5f ) ;
	float sign_original = signf_copy(original_triangle_area_signed, 1.0f ) ;
	
	float dA_dx1 = -y2*sign_over_two;
	float dA_dy1 =  x2*sign_over_two;
	float dA_dx2 =  y1*sign_over_two;
	float dA_dy2 = -x1*sign_over_two;

	float dA_dbx  = dA_dx1;
	float dA_dby  = dA_dy1;
	
	float dA_dcx  = dA_dx2;
	float dA_dcy  = dA_dy2;

	// invarianza translazionale
	float dA_dax  =  - dA_dx1 - dA_dx2;
	float dA_day  =  - dA_dy1 - dA_dy2;

	
	for(int loop=0; loop < total_loops ; loop++) {
      
	  int iy = first_y + loop / n_loops_x;
	  int ix = first_x + loop % n_loops_x;
      
	  float dy = ( iy_s - iy * OVS_F ) -0.5f; 
      
	  float p00_y = dy +  dil_p00_y  ;  
	  float p10_y = dy +  dil_p10_y  ;  
	  float p01_y = dy +  dil_p01_y  ;  
	  float p11_y = dy +  dil_p11_y  ;  
	  
	  float dx = ( ix_s - ix * OVS_F ) - 0.5f; 
	  
	  float p00_x = dx +  dil_p00_x  ;  
	  float p10_x = dx +  dil_p10_x  ;  
	  float p01_x = dx +  dil_p01_x  ;  
	  float p11_x = dx +  dil_p11_x  ;  
	  
	  if(  p00_x <= -0.5 * OVS_F && p10_x <= -0.5 * OVS_F && p01_x <= -0.5 * OVS_F &&  p11_x <= -0.5 * OVS_F ) {
	    continue;
	  }
	  
	  if(  p00_y <= -0.5 * OVS_F && p10_y <= -0.5 * OVS_F && p01_y <= -0.5 * OVS_F &&   p11_y <= -0.5 * OVS_F ) {
	    continue;
	  }
	  
	  if(  p00_x >= +0.5 * OVS_F && p10_x >= +0.5 * OVS_F && p01_x >= +0.5 * OVS_F  &&  p11_x >= +0.5 * OVS_F ) {
	    continue;
	  }
	  
	  if(  p00_y >= +0.5 * OVS_F && p10_y >= +0.5 * OVS_F && p01_y >= +0.5 * OVS_F  &&  p11_y >= +0.5 * OVS_F ) {
	    continue;
	  }
	  
	  float area_triangle = 0.0f;
	  float dt_dax = 0.0f, dt_day = 0.0f, dt_dbx = 0.0f, dt_dby = 0.0f;
	  float dt_dcx = 0.0f, dt_dcy = 0.0f;
	
	  // 00-11
	  float args[4] ;
	  float segno;

	  float fstart, fend ; 
	  
	  if( ax < bx ) {
	    args[0] = ax ; args[1] = ay ; args[2] = bx  ; args[3] = by ;
	    segno = 1.0f ; 
	  } else {
	    args[2] = ax ; args[3] = ay  ; args[0] = bx ; args[1] = by ;
	    segno = -1.0f ; 
	  }
	  
	  area_triangle +=  segno *integral_over_pixel( (args[0]+dx)*OVS, (args[1]+dy)*OVS, (args[2]+dx)*OVS, (args[3] +dy)*OVS, fstart, fend, path_x, path_y)   ;
	  add_to_area_der(  (dx+ ax)*OVS, (dy+ay)*OVS,  (dx+bx)*OVS, (dy+by)*OVS,  segno, fstart, fend,  dt_dax, dt_day, dt_dbx, dt_dby , path_x, path_y) ;
	  
	  // 00-11
	  if( bx < cx ) {
	    args[0] = bx ; args[1] = by ; args[2] = cx ; args[3] = cy ;
	    segno = 1.0f;
	  } else {
	    args[2] = bx ; args[3] = by ; args[0] = cx ; args[1] = cy ;
	    segno = -1.0f;
	  }
	  area_triangle +=  segno *integral_over_pixel( (args[0] +dx)*OVS, (args[1] +dy)*OVS, (args[2] +dx)*OVS, (args[3] +dy)*OVS, fstart, fend, path_x, path_y)   ;
	  add_to_area_der(  (dx+ bx)*OVS, (dy+by)*OVS, (dx+ cx)*OVS, (dy+cy)*OVS,  segno, fstart, fend,  dt_dbx, dt_dby, dt_dcx, dt_dcy, path_x, path_y ) ;

	  if( cx < ax ) {
	    args[0] = cx ; args[1] = cy ; args[2] = ax ; args[3] = ay ;
	    segno = 1.0f;
	  } else {
	    args[2] = cx ; args[3] = cy ; args[0] = ax ; args[1] = ay ;
	    segno = -1.0f;
	  }
	  area_triangle +=  segno *integral_over_pixel( (args[0]+dx)*OVS, (args[1]+dy)*OVS, (args[2]+dx)*OVS, (args[3]+dy)*OVS, fstart, fend, path_x, path_y)   ;
	  add_to_area_der(  (dx+cx)*OVS, (dy+cy)*OVS, (dx+ax)*OVS, (dy+ay)*OVS,  segno, fstart, fend,  dt_dcx, dt_dcy, dt_dax, dt_day, path_x, path_y ) ;
	  
	  if ( area_triangle !=0.0) {

	    int ipix = ix + iy*size1 * OVS;
	  
	    float fabs_at = fabs(area_triangle  );
	    float contribution =  0.5f*  fabs_at / original_triangle_area*  flat_field [ipix_s]  * my_Field_intensity * d_BUFFER[ipix];
	
            atomicAdd( &(der_Field_intensity_of_log[ipix_s]) , -  contribution * OVS_F * OVS_F  *  exp_deri_factor ); // aggiunto un segno meno perche si va direttamente su d_mGrad che
	                                                                                                           // da il steepset descent

	    // tengo conto della dilatazione nelle derivate
	    contribution = contribution * dila ; 
	    
	    float tmp  =  contribution / original_triangle_area;
	    float tmp2 =  contribution * sign_original/fabs_at * OVS_F;
	    
	    float dC_day = - dA_day  *tmp + dt_day * tmp2 ;
	    float dC_dax = - dA_dax  *tmp + dt_dax * tmp2 ;

	    float dC_dby = - dA_dby  *tmp + dt_dby * tmp2 ;
	    float dC_dbx = - dA_dbx  *tmp + dt_dbx * tmp2 ;

	    float dC_dcy = - dA_dcy  *tmp + dt_dcy * tmp2 ;
	    float dC_dcx = - dA_dcx  *tmp + dt_dcx * tmp2 ;


	    atomicAdd( &(der_pos_x[ (iy_s + 0) * ( size1 +1 ) + (ix_s + 0 )]),  dC_dax)    ; 
	    atomicAdd( &(der_pos_y[ (iy_s + 0) * ( size1 +1 ) + (ix_s + 0 )]),  dC_day)    ;	    
	    atomicAdd( &(der_pos_x[ (iy_s + 1) * ( size1 +1 ) + (ix_s + 1 )]),  dC_dbx)    ; 
	    atomicAdd( &(der_pos_y[ (iy_s + 1) * ( size1 +1 ) + (ix_s + 1 )]),  dC_dby)    ; 
	    
	    if( triangle == 0) {
	      atomicAdd( &(der_pos_x[ (iy_s + 0) * ( size1 +1 ) + (ix_s + 1 )]) , dC_dcx )   ; 
	      atomicAdd( &(der_pos_y[ (iy_s + 0) * ( size1 +1 ) + (ix_s + 1 )]) , dC_dcy )   ; 
	    } else {
	      atomicAdd( &(der_pos_x[ (iy_s + 1) * ( size1 +1 ) + (ix_s + 0 )]) ,  dC_dcx )  ; 
	      atomicAdd( &(der_pos_y[ (iy_s + 1) * ( size1 +1 ) + (ix_s + 0 )]) ,  dC_dcy )  ; 
	    }
	  }
	}
	delta_ratio=0  ;  
	my_Field_intensity=0  ;
	exp_deri_factor=0 ; 
      }
    }
  }
}



double calculate_gradient(float alpha,  cuErrGradArgs *cuArgs) {
  // this function calculates the error and optionally also the gradient at a given position alpha along the descent line P of conjugate gradient
  
  int OVS = cuArgs->exp_setup->OVS ;
  int OVS2 = OVS * OVS ;
  float OVS_F = 1.0 / OVS ; 

  float * d_my_phase_and_log ; 
  if(alpha) {
    d_my_phase_and_log = cuArgs->d_phase_and_logT_buffer;
    cudaMemcpy(d_my_phase_and_log  ,cuArgs ->d_phase_and_logT, sizeof(float) * (cuArgs->Npix  +  cuArgs->Npix ), cudaMemcpyDeviceToDevice );
    cublasSaxpy ( cuArgs->Npix  +  cuArgs->Npix ,  alpha, cuArgs->d_P_gc, 1, d_my_phase_and_log , 1);
  } else {
    d_my_phase_and_log = cuArgs->d_phase_and_logT;
  }

  CUDACHECK;	    
    
  CUDA_SAFE_CALL(cudaMemcpy( cuArgs->d_BUFFER ,cuArgs->d_radio_data_ovs  , sizeof(float) *  ( cuArgs->Npix * OVS2 ),  cudaMemcpyDeviceToDevice  ) ) ;
    
  cublasSscal(  cuArgs->Npix * OVS2 , -1.0  , cuArgs->d_BUFFER    , 1    ) ;
    
  CUDACHECK;	    

  {
    int NblocchiY =  iDivUp(  cuArgs->exp_setup->size0 +1 , 16 );
    int NblocchiX =  iDivUp(  cuArgs->exp_setup->size1 +1 , 16 );
    
    dim3  dimGrid  ( NblocchiX ,   NblocchiY );
    dim3  dimBlock (  16  ,  16   );
    
    int NblocksPerGrid=  min( 0xFFFF , (cuArgs->Npix+512-1) / 512 );
    
    
    der_N_central_kernel<<<dimGrid, dimBlock>>> (  cuArgs->d_pos_y, cuArgs->d_pos_x, d_my_phase_and_log , cuArgs->exp_setup->size0, cuArgs->exp_setup->size1) ;
      
    CUDACHECK;
    
    cublasSscal(  (  cuArgs->exp_setup->size0 + 1 )*( cuArgs->exp_setup->size1 + 1 ),
		  cuArgs->exp_setup->DB_EQUILIBRATION_FACTOR,
		  cuArgs->d_pos_y, 1    );
    cublasSscal(  (  cuArgs->exp_setup->size0 + 1 )*( cuArgs->exp_setup->size1 + 1 ),
		  cuArgs->exp_setup->DB_EQUILIBRATION_FACTOR,
		  cuArgs->d_pos_x, 1    );
    
    multM_N_kernel<<<dimGrid, dimBlock>>> (  cuArgs->d_pos_y, cuArgs->d_pos_x, cuArgs->d_M_phase2shift,  cuArgs->exp_setup->size0, cuArgs->exp_setup->size1) ;
    
    CUDACHECK;
    
    {
      CDV_kernel<<<NblocksPerGrid, 512>>> ( cuArgs->d_Field_intensity , d_my_phase_and_log + cuArgs->Npix , cuArgs->Npix) ;
      
      
      int imax ; 
      imax = cublasIsamax(  (cuArgs->exp_setup->size0 + 1 )*(cuArgs->exp_setup->size1 + 1 ),  cuArgs->d_pos_y  , 1)-1 ;
      CUDA_SAFE_CALL( cudaMemcpy( &(cuArgs->d_pos_max_y), cuArgs->d_pos_y + imax , sizeof(float) *1  , cudaMemcpyDeviceToHost ));
      
      imax = cublasIsamax(  (cuArgs->exp_setup->size0 + 1 )*(cuArgs->exp_setup->size1 + 1 ),  cuArgs->d_pos_x  , 1)-1 ;
      CUDA_SAFE_CALL( cudaMemcpy( &(cuArgs->d_pos_max_x), cuArgs->d_pos_x + imax , sizeof(float) *1  , cudaMemcpyDeviceToHost ));
      
      
    }
    
    CUDACHECK;

  
    {
      
      int NblocchiY_fill =  iDivUp(  cuArgs->exp_setup->size0  , TILE_SIZE ); 
      int NblocchiX_fill =  iDivUp(  cuArgs->exp_setup->size1 , TILE_SIZE );
      
      dim3  dimGrid  ( NblocchiX_fill ,   NblocchiY_fill );
      
      dim3  dimBlock (  TILE_SIZE  ,  TILE_SIZE   );
      
      int d_pos_max_y = ceilf( cuArgs->d_pos_max_y ) ; 
      int d_pos_max_x = ceilf( cuArgs->d_pos_max_x ) ; 

      
      fillBUFFER_kernel <<<dimGrid, dimBlock >>>(
								cuArgs->d_BUFFER,
								cuArgs->exp_setup->size0,
								cuArgs->exp_setup->size1,
							 
								cuArgs->d_flat_field ,
								cuArgs->d_Field_intensity ,
							 
								cuArgs->d_pos_y ,
								cuArgs->d_pos_x  ,
								cuArgs->n_spettro,
								cuArgs->d_spettro_data,
								OVS,
								cuArgs->exp_setup->spectral_binning 

                                                               );
							 
      CUDACHECK;
    }
  }
  
  ElementWiseMultSqrt( cuArgs->d_BUFFER  , cuArgs->d_flat_field      ,    cuArgs->Npix  , cuArgs->exp_setup->size0, cuArgs->exp_setup->size1  );
  
  cuArgs->errore  =  scalD( cuArgs ,  cuArgs->d_BUFFER  ,  cuArgs->d_BUFFER ,   cuArgs->Npix   )*0.5 ;
  
  ElementWiseMultSqrt( cuArgs->d_BUFFER  , cuArgs->d_flat_field      ,    cuArgs->Npix , cuArgs->exp_setup->size0, cuArgs->exp_setup->size1  );

  
  CUDACHECK;
  
  if(cuArgs->dograd) {
    
    
    
    CUDACHECK;
    {
      
      int NblocchiY_fill =  iDivUp(  cuArgs->exp_setup->size0  , TILE_SIZE );
      int NblocchiX_fill =  iDivUp(  cuArgs->exp_setup->size1  , TILE_SIZE );
      dim3  dimGrid  ( NblocchiX_fill ,   NblocchiY_fill );
      dim3  dimBlock (  TILE_SIZE  ,  TILE_SIZE   );
      
      CUDA_SAFE_CALL(cudaMemset( cuArgs->der_pos_x , 0 , sizeof(float) *  cuArgs->Npix * 2 ));
      CUDA_SAFE_CALL(cudaMemset( cuArgs->der_pos_y , 0 , sizeof(float) *  cuArgs->Npix * 2 ));
      
      useBUFFER_kernel <<<dimGrid, dimBlock >>>(
						cuArgs->d_BUFFER,
						cuArgs->exp_setup->size0,
						cuArgs->exp_setup->size1,
						
						cuArgs->d_flat_field ,
						cuArgs->d_Field_intensity ,
						
						cuArgs->d_pos_y ,
						cuArgs->d_pos_x ,
						
						cuArgs->d_mGrad + cuArgs->Npix    ,
						
						cuArgs -> der_pos_y,
						cuArgs -> der_pos_x,
						cuArgs->n_spettro,
						cuArgs->d_spettro_data,
						OVS,
						cuArgs->exp_setup->spectral_binning 
						) ;
      
      
      CUDACHECK;
    }

    {

      int NblocchiY =  iDivUp(  cuArgs->exp_setup->size0 +1 , 16 );
      int NblocchiX =  iDivUp(  cuArgs->exp_setup->size1 +1 , 16 );

      dim3  dimGrid  ( NblocchiX,   NblocchiY );
      dim3  dimBlock (  16  ,  16   );
      
      multM_T_kernel<<<dimGrid, dimBlock>>> (  cuArgs->der_pos_y, cuArgs->der_pos_x, cuArgs->d_M_phase2shift,  cuArgs->exp_setup->size0, cuArgs->exp_setup->size1) ;
      CUDA_SAFE_CALL(cudaMemset( cuArgs->d_tmp_laplacian ,0, sizeof(float) * cuArgs->Npix   ));

      
      der_T_central_kernel<<<dimGrid, dimBlock>>> (  cuArgs->der_pos_y, cuArgs->der_pos_x,  cuArgs->d_tmp_laplacian  , cuArgs->exp_setup->size0, cuArgs->exp_setup->size1) ;
      
    }
    
    
    cublasSaxpy (  cuArgs->Npix , -cuArgs->exp_setup->DB_EQUILIBRATION_FACTOR , cuArgs->d_tmp_laplacian  , 1, cuArgs->d_mGrad , 1);

    
    {
      // makes d/b constant
      float vr, vi , tmp;  
      float F = cuArgs->exp_setup->DBR_half/cuArgs->exp_setup->DB_EQUILIBRATION_FACTOR;
      {
	tmp = sqrt(F*F+1.0) ;
	vr = F/tmp;
	vi = 1.0/tmp;
      }
      int NblocksPerGrid=  min( 0xFFFF , (cuArgs->Npix+512-1) / 512 );
      
      projectGrad_kernel<<<NblocksPerGrid, 512>>>(    cuArgs->d_mGrad   ,
						      cuArgs->Npix,
						      vr,
						      vi
						      );
      
    }    
  }


  
 return     cuArgs->errore ;
 
}





class ErrorFunctor: public brent::func_base {
  // Brent is used only if minimisation by using the second derivative along the descent direction does not work.
  // For this case a wrap around the calculate_gradient function, implemented here,  is used by brent minimisation
 public:
  ErrorFunctor( cuErrGradArgs * CA ) {
    this-> Args = CA ;
  }
  double operator() (double x) {
    return  calculate_gradient( x ,  this->Args )   ;
    
  }
  cuErrGradArgs * Args;
};


double mybrent( float ax, float bx, float cx, float tol, cuErrGradArgs * Args, float *xmin) {
  // interface to the Brent algorithm by Brent
  
  ErrorFunctor err_funct( Args    ); 

  double my_xmin = 0;
  
  double res = brent::local_min((double) ax, (double) cx, (double) tol, err_funct, my_xmin    ) ;
  *xmin = my_xmin ; 
  return res;
  	 
}

float BRENT( cuErrGradArgs * CA,  double fa , float alpha) {
  // this functions does
  // -- a first bracketing of the minimum
  // -- a call to Brent's algorithm
  
  float ax,bx,cx ;
  double  fb ;
  ax = 0;
  
  CA->dograd=0;
  
  double tmp =  calculate_gradient( alpha, CA)  ;
  
  if( tmp < fa) {
    
    bx = alpha; fb = tmp ;
    int i=0;
    float alpha_c = 2*alpha;
    while((   calculate_gradient( alpha_c,  CA)      )<fb  ) {
      alpha_c *= 2 ;
      if( (++i)>20)  {
        break;	  
	// printf(" (++i)>20  alpha %e \n", alpha);
        // return alpha ; 
      }	  
    }
    cx = alpha_c;
    
  } else  {
    
    cx = alpha;  
    int i=0;
    float alpha_b = alpha/2;

    
    while((  fb = calculate_gradient( alpha_b,  CA )      )>fa  ) {
      alpha_b /= 2 ;
      
      
      // assert( (++i)<20) ;
      if ( (++i)>=20)  break ;   
    }
    bx = alpha_b;
  }
  float myalpha ; 
  double newerr = mybrent( ax,  bx,  cx,  0.0001 ,  CA,  &myalpha);

  CA->dograd = 1;
  
  double newerr2 =    calculate_gradient( myalpha,  CA)    ;
  
  printf("    CON BRENT SONO SCESO A %e ==  %e\n", newerr, newerr2);
  return myalpha;
}

float Advance_alpha( cuErrGradArgs * CA,   int  Npix,   float *d_P_gc, float festim )  {
  
  float maxf = 0.0;
  float tmp;
  
  int imax ; 
  
  imax = cublasIsamax(  Npix, d_P_gc, 1)-1 ;

  CUDA_SAFE_CALL( cudaMemcpy( &tmp,d_P_gc+imax , sizeof(float) *1  , cudaMemcpyDeviceToHost ));

  maxf = max( maxf, fabs(tmp) * CA->exp_setup->DB_EQUILIBRATION_FACTOR)   ; 
  
  imax = cublasIsamax(  Npix, d_P_gc+Npix, 1)-1 ;
  CUDA_SAFE_CALL( cudaMemcpy( &tmp,d_P_gc+Npix+imax , sizeof(float) * 1 , cudaMemcpyDeviceToHost ));
  maxf = max( maxf, fabs(tmp) ) ;
  
  float dalpha = festim  / maxf ;
  
  return dalpha ; 
}





double  aspect_CG(
		  int size0,
		  int size1,
		  float *data,
		  float *transm,
		  float DBR, // delta over beta, interally it will be divided by two (delta over absorption)
		  float SOURCE_Y, 
		  float SOURCE_X, 
		  float SOURCE_DISTANCE, // in meters
		  float DETECTOR_DISTANCE, // in meters
		  float IMAGE_PIXEL_SIZE,  // in micron, at the detector, not the voxel
		  float E_kev,
		  int NitersCG ,
		  float step_factor,
		  int scale_level_respect_to_expanded,
		  int verbose,
		  float *flat_field,
		  int n_spettro,
		  float *spettro_data,
		  int OVS,
		  int spectral_binning,
		  int salva_input) {




  AspectSetup *setup = new AspectSetup;
  {
    setup->K0  = 1.0e8*   2*M_PI  * E_kev /   12.398425 ;  // in cm^-1
    setup->size0 = size0  ; 
    setup->size1 = size1 ; 
    setup->DBR_half   = DBR/2  ; 
    setup->DB_EQUILIBRATION_FACTOR   = DBR/2  ; 

    setup->SOURCE_Y = SOURCE_Y ; 
    setup->SOURCE_X = SOURCE_X ; 
    setup->SOURCE_DISTANCE  =  SOURCE_DISTANCE; 
    setup->DETECTOR_DISTANCE  =  DETECTOR_DISTANCE; 
    setup->IMAGE_PIXEL_SIZE = IMAGE_PIXEL_SIZE ;
    setup->verbose = verbose ;
    setup->spectral_binning = spectral_binning;
    setup-> OVS = OVS;
  }

  init_M_phase2shift( setup ) ;

  AspectErrGradArgs *Args = new  AspectErrGradArgs;
  {
   Args->exp_setup = setup;
   Args->transm = transm;
   Args->radio_data = data;
   Args->spettro_data = spettro_data ;
   Args->n_spettro = n_spettro;
   if( flat_field == NULL ) {
     Args->flat_field  =  (float*) malloc( size0*size1* sizeof(float) ) ;
     {
       for(int i=0; i< size0*size1; i++) Args->flat_field[i] = 1.0;
     }
   } else {
     Args->flat_field = flat_field;
   }
  };



  int count=0;

  CUDACHECK;
  
  cuErrGradArgs * CA = gpu_Proj_calcGrad_ArgsConvert(  Args , scale_level_respect_to_expanded) ;
  
  CUDACHECK;

  CUDA_SAFE_CALL(cudaMemset( CA->d_mGrad,0,sizeof(float)*(CA->Npix+CA->Npix) ));
  double errore_tot = 1.0e32;
  double grad_norm, min_grad_norm;
  int patologico = 0;
  {
    cuErrGradArgs CA_tmp = *CA;

    {
      CA_tmp.dograd = 1  ;

      errore_tot = calculate_gradient( 0.0f,  &CA_tmp) ;
      printf(" EEE %e\n", errore_tot);

      // the minimum of the gradient norm is tracked in min_grad_norm over the loops.
      // it will be used to stop the algorithm if it happens that the gradient norm increase above ten times its minimum over the loops
      min_grad_norm = grad_norm = scalD( CA, CA->d_mGrad , CA->d_mGrad , (CA->Npix+CA->Npix) ) ;
      					 
    }
  }
  
  CUDA_SAFE_CALL(cudaMemcpy( CA->d_P_gc,CA->d_mGrad , sizeof(float) * (CA->Npix+CA->Npix) , cudaMemcpyDeviceToDevice ));

  int iter;

  // the two variables below as in the CG algorithm
  float beta = -1.0;
  float gg = -1.0;

  
  float initial_gg = -1.0;
  float old_error = errore_tot;
  
  int iloop=0;

  float fffact = 1.0; // this may possibly modulate the step for numerical derivatives
  int usebrent = 0;
  
  {
    int gradient_is_ready = 0;

    
    for(iter=0; iter<  NitersCG   ; iter++) {
      // the CG loops
      
      CUDA_SAFE_CALL(cudaMemcpy( CA->d_mGrad_old, CA->d_mGrad, sizeof(float) * (CA->Npix+CA->Npix), cudaMemcpyDeviceToDevice ));
      gg =  scalD( CA,   CA->d_mGrad_old , CA->d_mGrad_old , (CA->Npix+CA->Npix)   )   ;

      
      int kref;
      int Nref = 1;
      // The number of refinement of the minimum along the descent direction  CA->d_P_gc

      
      float tot_alpha = 0.0;
      // the total displacement along CA->d_P_gc

      for( kref =0; kref<Nref; kref++) {
	double speed1;
	if(! gradient_is_ready) {
	  CUDA_SAFE_CALL(cudaMemset( CA->d_mGrad ,0, sizeof(float) *  (CA->Npix+CA->Npix)  ));
	  {
	    CA->dograd=1;
	    errore_tot =  calculate_gradient(0.0f, CA ) ;
	    printf(" EEE %e\n", errore_tot);
	  }
	}

	{
	  speed1 = 0.0;

	  float dalpha = Advance_alpha(CA,  CA->Npix,  CA->d_P_gc,       step_factor * fffact)  ;
	  // above an estimation is done for a reasonable dalpha for the numerical second derivative 
	  

	  {
	    CUDA_SAFE_CALL(cudaMemcpy( CA->dmGda_along_p, CA->d_mGrad, sizeof(float) * (CA->Npix+CA->Npix), cudaMemcpyDeviceToDevice ));
	    cublasSscal(  (CA->Npix+CA->Npix) , -1.0f, CA->dmGda_along_p , 1    );
	    // now we have the gradient at alpha=0

	    
	  
	    cuErrGradArgs CA_tmp = *CA;
	    {
	      CA_tmp.d_mGrad  = CA->dmGda_along_p  ;  // the gradient at dalpha will be added in place to this vector
	      double tmp_err = calculate_gradient( dalpha,  &CA_tmp );
	    }
	    
	    cublasSscal(  (CA->Npix+CA->Npix) , 1.0f/(dalpha), CA->dmGda_along_p , 1    );
	    // no the difference vector is contained in  CA->dmGda_along_p, divided by dalpha this will give the derivative of mG along P

	    
	    speed1 += -1.0*     scalD( CA,  CA->dmGda_along_p , CA->d_P_gc , (CA->Npix+CA->Npix)   );
	    // this is the second derivative, respect to alpha, of the error
	    
	  }
	}
	
	double speed = -1.0*     scalD( CA,   CA->d_mGrad , CA->d_P_gc , (CA->Npix+CA->Npix)   );
	// this is the first derivative of the error.

      
	if(kref==0) {
	  if(!(speed<=0)) {
	    if(  CA->exp_setup->verbose  ){
	      printf( " WARNING speed>0 %e     beta gg %e %e  \n", speed,  beta, gg);
	    }
	    break;
	  }
	}
            
	float alpha ;	      
	alpha = -speed/(speed1) ;
	// by moving of alpha we aim to reach the minimum

	
	
	if(!usebrent ) {
	} else {
	  // if brent has been activated use brent, and alpha will be a guess
	  kref = Nref ;
	  alpha =    BRENT( CA, CA->errore  , alpha)    ; // non calcola gradienti
	}

	
	cublasSaxpy ( (CA->Npix+CA->Npix) , alpha, CA->d_P_gc, 1, CA->d_phase_and_logT , 1);
	// a displacement of alpha has now been applied
	
	tot_alpha += alpha;      
      }
      
      if ( patologico ) {
	printf(" esco patologico \n");
	// a pathological condition has been dedtected. It is time to stop.
	break;
      }

      
      
      CA->dograd = 1;
      CUDA_SAFE_CALL(cudaMemset( CA->d_mGrad ,0, sizeof(float) *  (CA->Npix+CA->Npix)  ));
      {
	// calculate error and gradient at the new position
	errore_tot =calculate_gradient( 0,  CA) ; 
	printf(" EEE %e\n", errore_tot);

	gradient_is_ready = 1;
      }
      double ngng =   scalD( CA,   CA->d_mGrad , CA->d_mGrad , (CA->Npix+CA->Npix)   )   ;

      grad_norm = ngng ;

      if( grad_norm < min_grad_norm) {
	min_grad_norm = grad_norm;
      } else {
	if( grad_norm  > 10 * min_grad_norm) {
	  // too much. The gradient norm is not necesseraly always decreasing, but we put a limit to its increase.
	  printf(" Risalita modulo grad. Preparo uscita  %e %e\n", grad_norm, min_grad_norm );
	  patologico = 2;
	}
      }
    
      if( ngng!=ngng ) {
	// we have a Nan. stop
	if( CA->exp_setup->verbose  ){
	  printf(" esco perche ngn!=ngn\n");
	}
	break;
      }
      
      if(errore_tot > old_error) {
	// another pathological condition
	// we are going to activate brent
	
	if(  CA->exp_setup->verbose  ){
	  printf(" WARNING  errore_tot > old %e %e %d  \n", errore_tot, old_error, iter );
	}
	
	if(fffact>0.01) fffact/=2.0;
	// smaller step for numerical second derivative
	
	//if(  ( errore_tot-old_error  )/errore_tot  > 0*1.0e-4 )
	{

	  
	  cublasSaxpy ( (CA->Npix+CA->Npix) ,  -tot_alpha  , CA->d_P_gc, 1, CA->d_phase_and_logT , 1);
	  // we have reverted the last step along P_gc
	  

	  if( CA->exp_setup->verbose ){
	    printf(" Reinizializzo P  errore_tot > old %e %e %d \n", errore_tot, old_error, iter );
	  }
	  {
	    count++;
	    // if(fffact>0.01) fffact/=2.0;
	    usebrent=1;
	    
	    CUDA_SAFE_CALL(cudaMemcpy( CA->d_mGrad, CA->d_mGrad_old, sizeof(float) * (CA->Npix+CA->Npix), cudaMemcpyDeviceToDevice ));
	    CUDA_SAFE_CALL(cudaMemcpy( CA->d_P_gc, CA->d_mGrad_old, sizeof(float) * (CA->Npix+CA->Npix), cudaMemcpyDeviceToDevice ));
	    // revert to the previous state, resettin P by the way.
	    
	    usebrent =1;
	    
	    if( count <10 && fffact>0.0000001) continue;
	    else  break;
	    // after ten usage of Brent, at smaller and smaller fffact, if error still does not decrease, give it up
	    
	  } 
	}
      } else {
	// everything is going well
	if(usebrent) {
	  // if we are using brent
	  usebrent+=1;
	  if(usebrent>3) {
	    // after using it three times if everything is going well
	    // we revert to simple refinement
	    usebrent=0;
	    count=0;
	  }
	}
      }
      // below, standard CG  steps
      old_error = errore_tot;
      
      double ngg  = scalD( CA,   CA->d_mGrad_old , CA->d_mGrad , (CA->Npix+CA->Npix)   )  ;
      
      beta = (ngng- ngg)/gg;
      
      cublasSscal(  (CA->Npix+CA->Npix), beta, CA->d_P_gc, 1    );
      cublasSaxpy ( (CA->Npix+CA->Npix) ,  1.0f  , CA->d_mGrad, 1, CA->d_P_gc , 1);
            
      if( CA->exp_setup->verbose ){
	printf("CG %d %e   beta gg ngng %e %e %e fffact %e  festim %e\n",  iter+iloop*NitersCG,  errore_tot, beta, gg, ngng, fffact, step_factor);
      }
      if(initial_gg == -1.0 ) initial_gg = gg ; 
      
      if(ngng< 1.0e-9 *initial_gg)  {
	if( CA->exp_setup->verbose ){
	  printf(" esco  perche ngng < 1.0e-9*initial_gg\n");
	}
	break;
      }
    }
    


  }
  
  // revert to fixed scale
  {
    int scale_level_respect_to_fixed = scale_level_respect_to_expanded;    // scale_level_respect_to_fixed e' rispetto a estensione naturale snza auxiliary_expansion
    if(AUXILIARY_EXPANSION) scale_level_respect_to_fixed=scale_level_respect_to_fixed-1;

    int Npix_orig = Args->exp_setup->size0* Args->exp_setup->size1  ; 
    float *tmp_qi =  (float *) malloc(  2* Npix_orig  * sizeof(float) ) ; 
    
    Rescale( CA->d_phase_and_logT , tmp_qi ,   Args->exp_setup->size0, Args->exp_setup->size1,  1, CA->d_Tmp_For_auxiliary_expansion, scale_level_respect_to_fixed, RESCALE_BACKWARD, REPLICATE_FOR_ABS );
    
    
    for(int i=0; i< Npix_orig; i++) {
      Args->transm[i] = exp( tmp_qi[Npix_orig+i] ) ; 
    }
    
    free(tmp_qi);


    if( CA->exp_setup->verbose  ){
      printf(" ESCO GPU_CG\n");
    }

    free_allocations(  CA );
    if( flat_field == NULL ) {
      free(Args->flat_field);
    }
  }
  
 
  delete  Args;
  free( setup->M_phase2shift);
  delete setup;
  
  
  return errore_tot ; 

}


