 
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY

#include<string>
#include <pybind11/numpy.h>
#include<exception>
#include"utilities.h"

double  aspect_CG(
		  int size0,
		  int size1,
		  float *data,
		  float *transm,
		  float DBR, // delta over beta, interally it will be divided by two (delta over absorption)
		  float SOURCE_Y, 
		  float SOURCE_X, 
		  float SOURCE_DISTANCE, // in meters
		  float DETECTOR_DISTANCE, // in meters
		  float IMAGE_PIXEL_SIZE,  // in micron, at the detector, not the voxel
		  float E_kev,
		  int NitersCG ,
		  float step_factor,
		  int reduction_factor,
		  int verbose,
		  float *rmd,
		  int n_spettro,
		  float *spettro_data,
		  int ovs,
		  int aggregate_period,
		  int salva_input
		  ) ;


using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword


// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(aspect_phase_retrieval,m)
{
  // xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for aspect_phase_retrieval";
  
  m.def("filter", []( py::array_t<float> &data,
		      py::array_t<float> &solution, 
		      float DBR,
		      float SOURCE_Y,
		      float SOURCE_X,
		      float SOURCE_DISTANCE,
		      float DETECTOR_DISTANCE,
		      float IMAGE_PIXEL_SIZE,
		      float E_kev,
		      int NitersCG,
		      float step_factor,
		      int reduction_factor,
		      int verbose,
		      py::array_t<float> &myff,
		      py::array_t<float> &spettro_data,
		      int ovs,
		      int aggregate_period,
		      int salva_input
		      )
		  {
		    
		    py::buffer_info data_info  =  nabuxx::checked_buffer_request(data , 'f' , "filter of aspect_phase_retrieval" , "data",  nabuxx::memlayouts::c_contiguous, 2 );
		    py::buffer_info solution_info  =  nabuxx::checked_buffer_request(solution , 'f' , "filter of aspect_phase_retrieval" , "solution",  nabuxx::memlayouts::c_contiguous, 2 );
		    
		    if(  data_info.shape[0] != solution_info.shape[0] || data_info.shape[1] != solution_info.shape[1]   ) {
		      std::stringstream ss;
		      ss << "The argumens data and solution,  to filter,  must have  the same shape but their shaper are " <<  data_info.shape[0] << "," <<  data_info.shape[1]  ;
		      ss << " and " <<  solution_info.shape[0] << "," <<  solution_info.shape[1]  ;
		      throw std::invalid_argument(ss.str() );
		    }
		    
		    py::buffer_info  myff_info = myff.request();
		    float *myff_ptr;
		    if (myff_info.ndim != -1) {
		      myff_info  =  nabuxx::checked_buffer_request(myff , 'f' , "filter of aspect_phase_retrieval" , "myff",  nabuxx::memlayouts::c_contiguous, 2 );
		      if(  data_info.shape[0] != myff_info.shape[0] || data_info.shape[1] != myff_info.shape[1]   ) {
			std::stringstream ss;
			ss << "The argumens data and myff,  to filter,  must have  the same shape but their shaper are " <<  data_info.shape[0] << "," <<  data_info.shape[1]  ;
			ss << " and " <<  myff_info.shape[0] << "," <<  myff_info.shape[1]  ;
			throw std::invalid_argument(ss.str() );
		      }
		      myff_ptr = (float*) myff_info.ptr ;
		    } else {
		      myff_ptr=NULL;
		    }
		    
		    py::buffer_info  spettro_data_info = spettro_data.request();
		    float *spettro_data_ptr;
		    spettro_data_info  =  nabuxx::checked_buffer_request(spettro_data , 'f' , "filter of aspect_phase_retrieval" , "spettro_data",  nabuxx::memlayouts::c_contiguous, 2 );
		    if(    spettro_data_info.shape[1] != 4   ) {
		      std::stringstream ss;
		      ss << "The argumens spettro_data must have dimension n_spettro x 4  where each quadruplet is formed by  " ;
		      ss << " ene_ratio beta_ratio delta_ratio fraction. But dimensions were " <<  spettro_data_info.shape[0] << "," <<  spettro_data_info.shape[1]  ;
		      throw std::invalid_argument(ss.str() );
		    }
		    spettro_data_ptr = (float*) spettro_data_info.ptr ;
		    int n_spettro = spettro_data_info.shape[0] ; 
		    
		    return aspect_CG(
				     data_info.shape[0],
				     data_info.shape[1],
				     (float*) data_info.ptr, 
				     (float*) solution_info.ptr, 
				     DBR,
				     SOURCE_Y,
				     SOURCE_X,
				     SOURCE_DISTANCE,
				     DETECTOR_DISTANCE,
				     IMAGE_PIXEL_SIZE,
				     E_kev,
				     NitersCG,
				     step_factor,
				     reduction_factor,
				     verbose,
				     myff_ptr,
				     n_spettro,
				     spettro_data_ptr,
				     ovs,
				     aggregate_period,
				     salva_input
				     ) ; 
		  },
	py::arg("data"),
	py::arg("solution"),
	py::arg("delta_beta_ratio") ,
	py::arg("source_y_px"),
	py::arg("source_x_px"),
	py::arg("source_distance_meters") ,
	py::arg("detector_distance_meters") ,
	py::arg("detector_pixel_size_micron") ,
	py::arg("e_kev") ,
	py::arg("niters_max")=400 ,
	py::arg("step_factor")=0.001,
	py::arg("reduction_factor")=1,
	py::arg("verbose")=0,
	py::arg("myff")=py::none(),
	py::arg("spettro_data")=py::none(),
	py::arg("ovs")=1,
	py::arg("aggregate_period")=100,
	py::arg("salva_input")=0
	);
     
}

