cmake_minimum_required(VERSION 3.16)

project (filter_zigzag  LANGUAGES CXX  C)
cmake_minimum_required(VERSION 3.8)
message(STATUS "Building for system processor ${CMAKE_SYSTEM_PROCESSOR}")
include(CheckCXXCompilerFlag)


find_package(PkgConfig REQUIRED)


find_package(pybind11  CONFIG)
message(STATUS  "pybind11   "  "${pybind11_VERSION}" )


if(DEFINED ENV{CXX_AUXILIARY_PACKAGES_DIR})
  set( CXX_DIR  "$ENV{CXX_AUXILIARY_PACKAGES_DIR}"   ) 
else()
  set( CXX_DIR  "/scisoft/tomotools_env"   ) 
endif()



if (  ${pybind11_VERSION} LESS 2.9   )
  message(STATUS  "setting manually pybind11_INCLUDE_DIRS")
  if(EXISTS "${CXX_DIR}/cxx_packages/src/pybind11/include")
    set( pybind11_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/pybind11/include"  "${pybind11_INCLUDE_DIRS}")
  else()
    message(FATAL_ERROR "the package pybind11 found, but it is too old, and the auxiliary sources have not been found in ${CXX_DIR}/cxx_packages/src/pybind11/include")
  endif()
  
endif()

find_package(xtensor QUIET)
find_package(xtensor-python QUIET)

message(STATUS  "searching xtensor has given xtensor_FOUND=${xtensor_FOUND}" )

if(NOT ${xtensor_FOUND})
  message(STATUS  "setting manually xtensor_INCLUDE_DIRS" )
  if(EXISTS "${CXX_DIR}/cxx_packages/src/xtensor/include")
    set( xtensor_INCLUDE_DIRS "${CXX_DIR}/cxx_packages/src/xtensor/include;${CXX_DIR}/cxx_packages/src/xtl/include")
  else()
    message(FATAL_ERROR "the package xtensor was not found, neither was the auxiliary directory with headers.")
  endif()
endif(NOT ${xtensor_FOUND})

message(STATUS  "searching xtensor-python has given xtensor_FOUND=${xtensor-python_FOUND}" )
if(NOT ${xtensor-python_FOUND})
  message(STATUS  "adding manually to xtensor_INCLUDE_DIRS" )
  if(EXISTS "${CXX_DIR}/cxx_packages/src/xtensor-python/include")
    set( xtensor_INCLUDE_DIRS   ${xtensor_INCLUDE_DIRS} ${CXX_DIR}/cxx_packages/src/xtensor-python/include)
  else()
    message(FATAL_ERROR "the package xtensor-python was not found, neither was the auxiliary directory with headers.")  
  endif()
endif(NOT ${xtensor-python_FOUND})


find_package(OpenMP  REQUIRED)

set(CMAKE_C_FLAGS " -Wall -O3 ${CMAKE_C_FLAGS} -g -Wuninitialized")
set(CMAKE_CXX_FLAGS " -Wall -O3  ${CMAKE_CXX_FLAGS} -g -Wuninitialized")


find_package(Python3)
execute_process(
  COMMAND "${Python3_EXECUTABLE}"
  -c "import numpy ; print(numpy.get_include())"
  OUTPUT_VARIABLE NUMPY_INCLUDE_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE
)
# Project scope; consider using target_include_directories instead
include_directories(
  BEFORE
  ${NUMPY_INCLUDE_DIR}
  )



add_subdirectory(filter_zigzag)
