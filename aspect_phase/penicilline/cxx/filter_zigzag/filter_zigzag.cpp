#include<algorithm>
#include<exception>
#include<stdexcept>

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include<xtensor/xadapt.hpp>

#include"filter_zigzag.h"
#include<string.h>
#include<omp.h>
#include<math.h>



void filtra_petri_dish(float * ima, float *condition, int dim0, int dim1, float abs_threshold) {
  // float std_threshold int std_radius, int min_neighbours) {

  float sigma = dim0/8.0 ;
  
#define isamax_xn( iy,ix) (   (factor*abs(ima[(ix+0)  + dim1*(iy+0)]) > abs_threshold)	\
			      &&  (   ima[(ix+0)+dim1*(iy+0)] *ima[(ix-1)+dim1*(iy+0)]   <0  ) \
			      && (   ima[(ix+0)+dim1*(iy+0)] *ima[(ix+1)+dim1*(iy+0)]   <0  ) \
			     )
  
#define isamax_yn( iy,ix) (   (factor*abs(ima[(ix+0)  + dim1*(iy+0)]) > abs_threshold)	\
			     && (   ima[(ix+0)+dim1*(iy+0)] *ima[(ix+0)+dim1*(iy-1)]   <0  ) \
			     && (   ima[(ix+0)+dim1*(iy+0)] *ima[(ix+0)+dim1*(iy+1)]   <0  ) \
			     )

#define isamax_x( iy,ix) (   (factor*abs(ima[(ix+0)  + dim1*(iy+0)]) > abs_threshold) \
			     &&  (   (ima[(ix+0)+dim1*(iy+0)] -ima[(ix-1)+dim1*(iy+0)])*(ima[(ix+0)+dim1*(iy+0)] -ima[(ix+1)+dim1*(iy+0)])   >0  ) \
			     )
  
#define isamax_y( iy,ix) (   (factor*abs(ima[(ix+0)  + dim1*(iy+0)]) > abs_threshold) \
			     &&  (   (ima[(ix+0)+dim1*(iy+0)] -ima[(ix+0)+dim1*(iy-1)])*(ima[(ix+0)+dim1*(iy+0)] -ima[(ix+0)+dim1*(iy+1)])   >0  ) \
			     )
  

  
  memset(condition, 0, dim0*dim1*sizeof(float) );
  for (int iy=2; iy< dim0-2; iy++) {
    float factor_y = (1 - exp( - (iy)*(iy)/sigma/sigma/2.0  ))*(1 - exp( - (dim0-iy)*(dim0-iy)/sigma/sigma/2.0  ));
    for (int ix=2; ix< dim1-2; ix++) {
      float factor = factor_y *(1 - exp( - (ix)*(ix)/sigma/sigma/2.0  ))*(1 - exp( - (dim1-ix)*(dim1-ix)/sigma/sigma/2.0  ));
      {
	if( isamax_xn( iy,ix) && ( isamax_x(iy, ix-1) || isamax_x(iy, ix+1)))  {
	  condition[ ix + dim1*iy ] = -1 ; 
	  // printf("SI: %e non e un max , %d\n",  ima[ix+dim1*iy], isamax_xn( iy,ix) );
	} else {
	  // printf(" %e non e un max, factor %e\n",  ima[ix+dim1*iy], factor);
	}
      }    
      {
	if( isamax_yn( iy,ix) && (isamax_y(iy-1, ix) || isamax_y(iy+1, ix+1)))  {
	  condition[ ix + dim1*iy ] = -1 ; 
	}
      }
    }
  }


  
  for (int iy = 0; iy < dim0; iy++) {
    int good = 0;
    int start, end ; 
    for (int ix = 0; ix < dim1; ix++) {
      if( condition[ ix + iy * dim1 ] == -1 ) {
	if (!good) start = ix ; 
	good = 1;
      } else {
	if( good) {
	  for(int i=start; i<ix; i++) {
	    condition[ i + iy * dim1 ] = (ix-start) ; 
	  }
	}
	good = 0;
      }
    }
  }
  
  for (int ix = 0; ix < dim1; ix++) {
    int good = 0;
    int start, end ; 
    for (int iy = 0; iy < dim0; iy++) {
      if( condition[ ix + iy * dim1 ] == -1 ) {
	if (!good) start = iy ; 
	good = 1;
      } else {
	if( good) {
	  for(int i=start; i<iy; i++) {
	    condition[ ix + i * dim1 ] = (i-start) ; 
	  }
	}
	good = 0;
      }
    }
  }

  for (int ix = 0; ix < dim1; ix++) {
    for (int iy = 0; iy < dim0; iy++) {
      if( condition[ ix + iy * dim1 ] > 1 )  condition[ ix + iy * dim1 ]  = 1;
      else condition[ ix + iy * dim1 ] = 0 ; 
    }
  }
  
  
  
  for (int iy = 1; iy < dim0-1; iy++) {
    for (int ix = -1; ix < dim1-1; ix++) {
      if( ! condition[ (ix +0 )  + dim1*(iy+0) ]) {
	for(int dy=-1; dy<2; dy++) {
	  for(int dx=-1; dx < 2; dx++) {
	    if (     condition[ (ix + dx )  + dim1*(iy+dy) ] ==1       ) {
	      condition[ (ix +0 )  + dim1*(iy+0) ]  =2 ; 
	    }
	  }
	}
      }
    }
  }
}


