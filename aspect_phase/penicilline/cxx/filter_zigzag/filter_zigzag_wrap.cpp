#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY
#include <xtensor-python/pyarray.hpp>

  
#include<vector>
#include<exception>
#include<string>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <xtensor/xarray.hpp>
#include <xtensor/xtensor.hpp>
#include<xtensor/xadapt.hpp>
#include"filter_zigzag.h"
#include"utilities.h"
using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword

// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(filter_zigzag,m)
{
  xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for ccd module (ccd filter and log)";

  m.def("filtra_petri_dish",  [](
				 py::buffer  & image,
				 py::buffer  & condition,
				 float abs_threshold
			     )  {
	   
	  py::buffer_info buffer_info  =   checked_buffer_request( image,  'f' ,  "filtra_petri_dish" , "image",  memlayouts::c_contiguous | memlayouts::f_contiguous       );
	  py::buffer_info condition_info  =   checked_buffer_request( condition,  'f' ,  "filtra_petri_dish" , "condition",  memlayouts::c_contiguous | memlayouts::f_contiguous       );
	  py::gil_scoped_release release;

	  if ( buffer_info.ndim!=2 || condition_info.ndim!=2  ) {
	    // std::vector<long int> new_shape = {1,   buffer_info.shape[0]  ,  buffer_info.shape[1] };
	    // this_obj.median_clip_correction_multiple_images( (float*)buffer_info.ptr,  new_shape ) ;
	    throw std::runtime_error("Input image must have 2  dimensions");
	  }
	  printf(" filtra_petri_dish %ld\n",  buffer_info.size ) ;
	  filtra_petri_dish( (float*)buffer_info.ptr,  (float*)condition_info.ptr,  buffer_info.shape[0], buffer_info.shape[1], abs_threshold ) ;
	}
	)
    ;
  
}

