import argparse
import os
import re
import h5py
import numpy as np


def get_arguments():
    parser = argparse.ArgumentParser(description="foo")
    parser.add_argument(
        "--original_file",
        required=True,
        help="The nexus file that we are going to duplicate with another file where entryxxxx/instrument/detector/data is replace by a real storage float32",
    )
    parser.add_argument("--target_file", required=True, help="The new nexus filename that we are going to create ")
    parser.add_argument("--entry_name", required=False, help="entry_name", default="entry0000")
    parser.add_argument(
        "--first",
        required=False,
        help="numeric index for the start. Optional. By default the file names are take as such. Otherwise as templates with XX..X  numerical part",
        default=None,
        type=int,
    )
    parser.add_argument(
        "--last",
        required=False,
        help="numeric index for the last. Optional. By default the file names are take as such. Otherwise as templates with XX..X  numerical part",
        default=None,
        type=int,
    )

    return parser.parse_args()


def duplicator(args, original_file, target_file):
    target_dir = os.path.dirname(target_file)
    os.makedirs(target_dir, exist_ok=True)

    with h5py.File(original_file, "r") as source_fp:
        with h5py.File(target_file, "w") as target_fp:
            source_fp.copy(args.entry_name, target_fp)

    with h5py.File(target_file, "r+") as f:
        shape = f[f"/{args.entry_name}/instrument/detector/data"].shape

        del f[f"/{args.entry_name}/instrument/detector/data"]
        f[f"/{args.entry_name}/instrument/detector/"].create_dataset("data", shape=shape, dtype=np.float32)
        f[f"/{args.entry_name}/instrument/detector/"].create_dataset(
            "retrieval_errors", shape=shape[:1] + (12,), dtype=np.float32
        )
        print(" forcing target dataset to have full shape ")
        f[f"/{args.entry_name}/instrument/detector/data"][-1,-1,-1] = 0.0
        image_keys = f[f"/{args.entry_name}/instrument/detector/image_key"][()]
        for i, ctrl in enumerate(image_keys):
            if ctrl == 1:
                f[f"/{args.entry_name}/instrument/detector/data"][i] = 1.0


def templator(filename_template, iz):
    pattern = re.compile("[X]+")
    # X represent the variable part of the 'template'
    # for example if we want to treat scans HA_2000_sample_0000.nx, ..., HA_2000_sample_9999.nx then
    # we expect the template to be HA_2000_sample_XXXX.nx
    # warning: If the dataset base names contains several X substrings the longest ones will be taken.
    ps = pattern.findall(filename_template)
    ls = list(map(len, ps))
    idx = np.argmax(ls)
    if len(ps[idx]) < 2:
        message = f""" The argument filename_template should contain  a substring  formed by at least two 'X'
        The filename_template was {filename_template}
        """
        raise ValueError(message)
    name_template = filename_template.replace(ps[idx], "{iz:" + "0" + str(ls[idx]) + "d}")
    return name_template.format(iz=iz)


def main():
    args = get_arguments()
    if os.path.exists(f"{args.target_file}"):
        message = f""" target file {args.target_file} exists. Refusing to overwrite it."""
        raise RuntimeError(message)
    if args.first is None:
        duplicator(args, args.original_file, args.target_file)
    else:
        for iz in range(args.first, args.last + 1):
            duplicator(args, templator(args.original_file, iz), templator(args.target_file, iz))

    return 0


if __name__ == "__main__":
    main()
